# Contributor: Łukasz Jendrysik <scadu@yandex.com>
# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Dan Theisen <djt@hxx.in>
pkgname=chrony
pkgver=4.1
pkgrel=0
pkgdesc="NTP client and server programs"
url="https://chrony.tuxfamily.org"
pkgusers="$pkgname"
pkggroups="$pkgname"
arch="all"
options="!check"  # Test suite no longer functions after 2020-01-01.
license="GPL-2.0-only"
depends=""
checkdepends="bash"
makedepends="asciidoctor libcap-dev libedit-dev texinfo"
install="$pkgname.pre-install $pkgname.pre-upgrade"
subpackages="$pkgname-doc $pkgname-openrc"
source="https://download.tuxfamily.org/$pkgname/$pkgname-$pkgver.tar.gz
	fix-tests.patch
	max_resolve_interval.patch
	chronyd.confd
	chronyd.initd
	chrony.logrotate
	chrony.conf
	timepps.h
	"

prepare() {
	default_prepare

	# We copy timepps.h to the local build directory instead of
	# creating a pps-tools-dev package for ppstime.h
	# (See https://github.com/ago/pps-tools)
	#
	# Adelie TODO: This is originally an Alpine hack, however it looks like
	# the pps-tools upstream is currently dead, we should determine if this
	# is the right solution going forward.
	mkdir -p pps-tools/sys
	cp "$srcdir"/timepps.h pps-tools/sys/
}

build() {
	CPPFLAGS="$CPPFLAGS -I./pps-tools/" ./configure \
		--prefix=/usr \
		--infodir=/usr/share/info \
		--mandir=/usr/share/man \
		--sysconfdir=/etc/$pkgname \
		--without-readline \
		--with-user=$pkgname \
		--with-sendmail=/usr/sbin/sendmail \
		--enable-ntp-signd
	make all docs
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install

	mkdir -p "$pkgdir"/etc/logrotate.d
	install -Dm644 "$srcdir"/chrony.logrotate "$pkgdir"/etc/logrotate.d/chrony

	mkdir -p "$pkgdir"/usr/share/doc/chrony
	install -m644 examples/*.example "$pkgdir"/usr/share/doc/chrony/
	install -Dm755 "$srcdir"/chronyd.initd "$pkgdir"/etc/init.d/chronyd
	install -Dm644 "$srcdir"/chronyd.confd "$pkgdir"/etc/conf.d/chronyd

	mkdir -p "$pkgdir"/var/lib/chrony \
		"$pkgdir"/etc/chrony

	# allow chrony to write logs, and wheel users to read them
	install -dm2750 -o $pkgusers -g wheel "$pkgdir"/var/log/chrony

	# chrony shouldn't be able to modify its own config
	chown $pkgusers:$pkggroups "$pkgdir"/etc/chrony
	install -m644 -o root -g root "$srcdir"/chrony.conf \
		"$pkgdir"/etc/chrony/chrony.conf

	# chrony.drift must be writable by chrony user
	touch "$pkgdir"/var/lib/chrony/chrony.drift
	chown -R $pkgusers:$pkggroups "$pkgdir"/var/lib/chrony
}

sha512sums="5e283d6a56e6852606c681a7c29c5786b102d584178cbd7033ebbc95a8e95533605631363b850a3087cca438a5878db7a317f120aab2fd856487d02fccfbcb1f  chrony-4.1.tar.gz
067d47224a8c075ec8f63ffc58e65b030fdf228a72c4f03d50a2f2c17414da65bb5d27c7c2e4ba99e909f452041db83eaebe3c9e34c0c8fce18e05ebb489735e  fix-tests.patch
b26581ed32680585edea5b8163a0062a87f648394c0f363c77a7d01a36608fcf4d005d9e6ab179ed2827b8a08f598f7bad4801bb5e135cad5107eb77fb19b247  max_resolve_interval.patch
1ebb72341b4421060a43d6db164d743de9144f1c23370e052f0db92a17e5e34f24ac98baddeb5dbfef27f67479142d448c4401d927394e55024760730a27e8de  chronyd.confd
9a18c9744d7b78d9b9be3f0ae0b43cb988b294c342764802e0d5df835d450e748584c0b345f7161f14d84e15c8bbd491514ee9dc45e4a65f1276c52124d2e312  chronyd.initd
ab38f06bf45888846778ad935e24abb30d13b6805e9a750bc694ff953695fa8c5b33aac560f5f7f96dc46031c1a38660e5c418b6fce6fb34a87908a9a3c99357  chrony.logrotate
a0fc89a742217f623d3dbd878dce57d994185d65f7a16b4c2b54a131c9d78b981d67aaa67dc87d978bb82e53798a492dac58b93bf94d378d858eb29fc2113e67  chrony.conf
eb11fc19243d1789016d88eb7645bfe67c46304547781489bf36eb1dd4c252d523681ff835a6488fa0ef62b6b9e2f781c672279f4439f5d5640a3f214a113048  timepps.h"
