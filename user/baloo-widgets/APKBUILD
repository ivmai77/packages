# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=baloo-widgets
pkgver=22.04.2
pkgrel=0
pkgdesc="Widgets that utilise the Baloo desktop indexing engine"
url="https://www.KDE.org/"
arch="all"
options="!check"  # Requires /etc/fstab to exist, and udisks2 to be running.
license="GPL-2.0+"
depends=""
depends_dev="qt5-qtbase-dev baloo-dev kio-dev"
makedepends="$depends_dev cmake extra-cmake-modules kconfig-dev
	kfilemetadata-dev ki18n-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/baloo-widgets-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="b85e7daf97b5f6b28423664c14875ce24affd3ef3e8fcea1d663f610dcecbed6b195bee12ff7c0c97cffe9fec6fabc30dbd137135d056f5b4061dd400aa55738  baloo-widgets-22.04.2.tar.xz"
