# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=lskat
pkgver=22.04.2
pkgrel=0
pkgdesc="Lieutenant Skat, an interactive two-player card game"
url="https://games.kde.org/game.php?game=lskat"
arch="all"
license="GPL-2.0-only"
depends="libkdegames-carddecks"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtdeclarative-dev
	qt5-qtsvg-dev kconfig-dev kconfigwidgets-dev kcoreaddons-dev kcrash-dev
	kdbusaddons-dev kdoctools-dev kguiaddons-dev ki18n-dev kxmlgui-dev
	kwidgetsaddons-dev libkdegames-dev phonon-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/lskat-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="363babec655cd7d7b1e51cf9ee3613a9914ea8a22adc10997b958b96f2e36f12082fe22266b63fac2ec17c103d224c870a1516287d56088a98dd05d8766afd5e  lskat-22.04.2.tar.xz"
