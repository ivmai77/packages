# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kcoreaddons
pkgver=5.94.0
pkgrel=0
pkgdesc="Core KF5 framework"
url="https://www.kde.org/"
arch="all"
options="!check"  # Needs more than 8192 open fds
license="LGPL-2.0+ AND (LGPL-2.1-only OR LGPL-3.0-only)"
depends=""
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev cmake extra-cmake-modules qt5-qttools-dev doxygen
	shared-mime-info"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kcoreaddons-$pkgver.tar.xz
	utf8.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="b94ffa2e845dcfb481d496348969b1ec4ddba0a3c1fdcec636567a89fb6f65179a10b43add14cf5c4e720dce4459e725dca6ee5800fdf9551385ec7fffc1b462  kcoreaddons-5.94.0.tar.xz
d462866912d9cc9a768477d872142eb67be6b616c044436a27aa71577546efe4aa323b3dac913f9dbb52fc62dbe27d464b30ac3c4cb23c5d7c414d96138e9300  utf8.patch"
