# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=ksystemlog
pkgver=22.04.2
pkgrel=0
pkgdesc="Friendly, powerful system log viewer tool"
url="https://www.kde.org/applications/system/ksystemlog/"
arch="all"
options="!check"  # Tests require X11
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev kxmlgui-dev kconfig-dev
	kcoreaddons-dev kwidgetsaddons-dev kitemviews-dev kio-dev karchive-dev
	kdoctools-dev ki18n-dev kcompletion-dev ktextwidgets-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/ksystemlog-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="6b62d0629246bc855b603fc8bf3b15333226e30a01ca843080caec583d2beb45647f8eacc920d836f630dabc657ad811ddc15f26a74341f6b71a722ace201804  ksystemlog-22.04.2.tar.xz"
