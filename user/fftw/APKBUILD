# Contributor: Ariadne Conill <ariadne@dereferenced.org>
# Maintainer: Zach van Rijn <me@zv.io>
pkgname=fftw
pkgver=3.3.10
_pkgver=$(printf '%s' "$pkgver" | sed 's/p/-pl/')
pkgrel=0
pkgdesc="Fastest fourier transform in the west"
url="http://www.fftw.org/"
# s390x: hangs on container and KVM
arch="all !s390x"
license="GPL-2.0+"
makedepends="texinfo"
# order of the libs split functions are important because of lib naming
subpackages="$pkgname-dev $pkgname-doc $pkgname-single-libs:single
	$pkgname-long-double-libs:long_double $pkgname-double-libs:double"
source="http://www.fftw.org/$pkgname-$_pkgver.tar.gz
	clock-sgi-cycle.patch
	"
builddir="$srcdir/$pkgname-$_pkgver"

_precision="single double long-double"

prepare() {
	default_prepare

	local i; for i in $_precision; do
		cp -r "$builddir" $srcdir/$i
	done
}

build() {
	local _openmp=

	local i; for i in $_precision; do
		case "$i" in
			single) _cf="--enable-single";;
			double) _cf="";;
			long-double) _cf="--enable-long-double";;
		esac

		case "$i--$CARCH" in
			single--x86_64 | double--x86_64)
				_cf="$_cf --enable-sse2 --enable-avx";;
			single--arm* | single--aarch64)
				_cf="$_cf --enable-neon";;
		esac

		msg "Building for $i precision ($_cf)"
		cd "$srcdir"/$i
		./configure \
			--build=$CBUILD \
			--host=$CHOST \
			--prefix=/usr \
			--sysconfdir=/etc \
			--mandir=/usr/share/man \
			--infodir=/usr/share/info \
			--enable-shared \
			--enable-threads \
			$_openmp \
			$_cf
		make
	done
}

check() {
	local i; for i in $_precision; do
		cd "$srcdir"/$i
		make check
	done
}

package() {
	local i; for i in $_precision; do
		cd "$srcdir"/$i
		make DESTDIR="$pkgdir" install
	done
}

single() {
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libfftw3f*.so* \
		"$subpkgdir"/usr/lib/
}

double() {
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libfftw3*.so* \
		"$subpkgdir"/usr/lib/
}

long_double() {
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libfftw3l*.so* \
		"$subpkgdir"/usr/lib/
}

sha512sums="2d34b5ccac7b08740dbdacc6ebe451d8a34cf9d9bfec85a5e776e87adf94abfd803c222412d8e10fbaa4ed46f504aa87180396af1b108666cde4314a55610b40  fftw-3.3.10.tar.gz
7683b31fd56c1883fd9ac553ced161b0e253163a97fb9f718f830da93ff1e45692dae4ddda3d9f3f238b8e8047f2cd62bd1933b67839328ccf386137f592f5b0  clock-sgi-cycle.patch"
