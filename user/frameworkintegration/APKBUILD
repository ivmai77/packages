# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=frameworkintegration
pkgver=5.94.0
pkgrel=0
pkgdesc="Framework providing components to allow applications to integrate with a KDE Workspace"
url="https://www.kde.org/"
arch="all"
options="!check"  # All tests require X11.
license="LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev cmake extra-cmake-modules python3 knotifications-dev
	kconfig-dev kconfigwidgets-dev kiconthemes-dev qt5-qtx11extras-dev
	knewstuff-dev kpackage-dev kxmlgui-dev"
subpackages="$pkgname-dev"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/frameworkintegration-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="3900fe970d459707b634df90515e49593632e7fe1dc9ba2a54b45d5042a768997f11b0875fe5419912673d8c9166e244d27b943b83c2d4a8936b8d6e30e17eb1  frameworkintegration-5.94.0.tar.xz"
