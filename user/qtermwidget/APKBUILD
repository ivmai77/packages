# Contributor: Síle Ekaterin Liszka <sheila@vulpine.house>
# Maintainer: Síle Ekaterin Liszka <sheila@vulpine.house>
pkgname=qtermwidget
pkgver=1.2.0
_lxqt=0.12.0
pkgrel=0
pkgdesc="Qt-based terminal widget, used in QTerminal"
url="https://lxqt.github.io/"
arch="all"
options="!check"  # No test suite.
license="GPL-2.0+"
depends=""
depends_dev="libutempter-dev"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qttools-dev
	lxqt-build-tools>=$_lxqt $depends_dev"
subpackages="$pkgname-dev"
source="https://github.com/lxqt/qtermwidget/releases/download/$pkgver/qtermwidget-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DQTERMWIDGET_USE_UTEMPTER=True \
		${CMAKE_CROSSOPTS} -Bbuild
	make -C build
}

check() {
        CTEST_OUTPUT_ON_FAILURE=TRUE make -C build test
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="0424720144aca1d6b463bff0f79935b63bc8902dbc169901b2d0f4a8df1dd73ea1b0224fd3d69049cb9d1500fbab8eb349212c1c967a4188f25f7c0a2330bd5a  qtermwidget-1.2.0.tar.xz"
