# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kpackage
pkgver=5.94.0
pkgrel=0
pkgdesc="Frameworks for managing KDE data packages"
url="https://www.kde.org/"
arch="all"
options="!check"  # Requires Plasma to be installed, causing circular dep
license="LGPL-2.0+"
depends=""
depends_dev="qt5-qtbase-dev karchive-dev ki18n-dev kcoreaddons-dev"
docdepends="kcoreaddons-doc"
makedepends="$depends_dev cmake extra-cmake-modules python3 kdoctools-dev
	qt5-qttools-dev doxygen graphviz $docdepends"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kpackage-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="65c58a01c302e3f5ad89fab1bec3b9bf1f17a9a94408b565137a1cfb4f083436c39e41b57c11449d5f764252df86eb7e9b4a08b86d65492f0f7eb1899b8d2cc7  kpackage-5.94.0.tar.xz"
