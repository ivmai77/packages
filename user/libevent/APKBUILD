# Contributor: Sergey Lukin <sergej.lukin@gmail.com>
# Maintainer: Síle Ekaterin Liszka <sheila@vulpine.house>
pkgname=libevent
pkgver=2.1.13_pre1
pkgdate=20220114
pkgrel=0
pkgdesc="An event notification library"
url="https://libevent.org/"
arch="all"
license="BSD-3-Clause"
options="!check" # see #448
depends=""
depends_dev="python3 autoconf automake libtool"
makedepends="$depends_dev openssl-dev"
subpackages="$pkgname-dev"
#source="https://github.com/$pkgname/$pkgname/releases/download/release-$pkgver-stable/$pkgname-$pkgver-stable.tar.gz
source="https://distfiles.adelielinux.org/source/$pkgname-$pkgver-$pkgdate.tar.gz
	py3_rpcgen.patch
	"

# secfixes:
#   2.1.8-r0:
#   - CVE-2016-10195
#   - CVE-2016-10196
#   - CVE-2016-10197

builddir="$srcdir"/$pkgname-$pkgver-$pkgdate

prepare() {
	./autogen.sh
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--disable-static
	make
}

check() {
	make check
}

package() {
	make -j1 DESTDIR=$pkgdir install
}

dev() {
	replaces="libevent"
	default_dev
	provides="$provides pc:libevent=$pkgver-r$pkgrel"
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr/
}

sha512sums="1b5a7d71e090d3e3d76b7b8e83ae99d3b1430751f2864ac60ae6b2a0d18e8557122e63f6f21c41b4e71c97ea54dc04dc425f81adafd50146387cf92e60aec7ff  libevent-2.1.13_pre1-20220114.tar.gz
ca097528f88b0a86248be594b44eaa7edcb3a1ee2b808685c09aa7947bb5c10342f4b584e7f2fcef6bc4a185fecb80da4a7a6660dd5c075f3416f9a55a1389b0  py3_rpcgen.patch"
