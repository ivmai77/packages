# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kpimtextedit
pkgver=22.04.2
pkgrel=0
pkgdesc="Text editor for Personal Information Managers (PIMs)"
url="https://pim.kde.org/"
arch="all"
options="!check"  # Test suite requires X11 running.
license="LGPL-2.1+ AND GPL-2.0+"
depends=""
depends_dev="qt5-qtbase-dev kwidgetsaddons-dev syntax-highlighting-dev
	ktextwidgets-dev sonnet-dev"
makedepends="$depends_dev cmake extra-cmake-modules qt5-qttools-dev ki18n-dev
	grantlee-dev kcodecs-dev kconfig-dev kconfigwidgets-dev kcoreaddons-dev
	kdesignerplugin-dev kemoticons-dev kiconthemes-dev kio-dev kxmlgui-dev
	qt5-qtspeech-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kpimtextedit-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="68a89125dbd4c45b2db825af3cea3c8bf38028f26eca021fc61d8982f25ce33722f0da3cf1d8f0f0f4b9d2e8e5613f0af6010756f064e89fabd305a905521443  kpimtextedit-22.04.2.tar.xz"
