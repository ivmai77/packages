/* src/config.h.  Generated from config.h.in by configure.  */
/* src/config.h.in.  Generated from configure.ac by autoheader.  */

/* Name of package */
#define SCROT_PACKAGE "scrot"

/* Define to the address where bug reports for this package should be sent. */
#define PACKAGE_BUGREPORT "https://github.com/resurrecting-open-source-projects/scrot/issues"

/* Define to the full name of this package. */
#define PACKAGE_NAME "scrot"

/* Define to the full name and version of this package. */
#define PACKAGE_STRING "scrot 1.4"

/* Define to the one symbol short name of this package. */
#define PACKAGE_TARNAME "scrot"

/* Define to the home page for this package. */
#define PACKAGE_URL ""

/* Define to the version of this package. */
#define PACKAGE_VERSION "1.4"

/* Version number of package */
#define SCROT_VERSION "1.4"

/* Define to empty if `const' does not conform to ANSI C. */
/* #undef const */
