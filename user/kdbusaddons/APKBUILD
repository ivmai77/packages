# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kdbusaddons
pkgver=5.94.0
pkgrel=0
pkgdesc="Framework for coping with D-Bus"
url="https://www.kde.org/"
arch="all"
options="!check"  # Requires running dbus-daemon
license="LGPL-2.0+"
depends=""
depends_dev="qt5-qtbase-dev qt5-qtx11extras-dev"
makedepends="$depends_dev cmake extra-cmake-modules qt5-qttools-dev doxygen"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kdbusaddons-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="8075330a54c41a94ad97b2514635ad3845128dd856510ebd4441ee3d349c125ac189cf4d3d582dd5e19cef29f07e7df8d22f4f72c8b3fe26d4ca5e262c527929  kdbusaddons-5.94.0.tar.xz"
