# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libkexiv2
pkgver=22.04.2
pkgrel=0
pkgdesc="KDE integration with Exiv2 EXIF data library"
url="https://www.KDE.org/"
arch="all"
license="GPL-2.0+"
depends=""
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev cmake extra-cmake-modules exiv2-dev"
subpackages="$pkgname-dev"
source="https://download.kde.org/stable/release-service/$pkgver/src/libkexiv2-$pkgver.tar.xz
	backport.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="b0698d268a88cf546a4cb028adc26170297e5c79e63e5d6cfcb1500292e7f73697b322055e31594c218b037db33447b68613c128a8c0baca0779fc973a5f6c8e  libkexiv2-22.04.2.tar.xz
b2623cd9ff92b7a86a4e9e1fbb691c1d548482a32e568e06936de48703ff80d927fa3b25779f63530875388c4e7fb94d6045beabae9ac043c652fc8826d3250c  backport.patch"
