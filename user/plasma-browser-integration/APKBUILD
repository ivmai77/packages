# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=plasma-browser-integration
pkgver=5.24.5
pkgrel=0
pkgdesc="Integrate Web browsers into the KDE Plasma desktop"
url="https://www.kde.org/"
arch="all"
license="GPL-3.0+"
depends=""
makedepends="cmake extra-cmake-modules kactivities-dev kconfig-dev kcrash-dev
	kdbusaddons-dev kfilemetadata-dev ki18n-dev kio-dev knotifications-dev
	krunner-dev plasma-workspace-dev purpose-dev qt5-qtbase-dev"
subpackages="$pkgname-lang"
source="https://download.kde.org/stable/plasma/$pkgver/plasma-browser-integration-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
	rm -r "$pkgdir"/etc
}

sha512sums="f34885920f1d4859d57ce3c96523c46d392f5835bce6b7d73afb30ea60827e5d520c168ccf45d2d46cec8027b0d9abe2bfc0c5b4ecfbf3b4433bb3ed0b405497  plasma-browser-integration-5.24.5.tar.xz"
