# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Contributor: Síle Ekaterin Liszka <sheila@vulpine.house>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=fontforge
pkgver=20220308
pkgrel=0
pkgdesc="Open source font editor"
url="https://fontforge.github.io/en-US/"
arch="all"
license="GPL-3.0+ AND BSD-3-Clause"
depends="desktop-file-utils shared-mime-info"
makedepends="cmake freetype-dev giflib-dev glib-dev libjpeg-turbo-dev
	libuninameslist-dev libpng-dev libspiro-dev libx11-dev libxi-dev 
	libxml2-dev pango-dev python3-dev tiff-dev zlib-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://github.com/fontforge/fontforge/releases/download/$pkgver/fontforge-$pkgver.tar.xz"

build() {
        if [ "$CBUILD" != "$CHOST" ]; then
                CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
        fi
        cmake -B build \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DENABLE_GUI=ON \
		-DENABLE_X11=ON \
		-DENABLE_PYTHON_SCRIPTING=ON \
		-DENABLE_PYTHON_EXTENSION=ON \
		-DENABLE_LIBGIF=ON \
		-DENABLE_LIBJPEG=ON \
		-DENABLE_LIBPNG=ON \
		-DENABLE_LIBTIFF=ON \
		${CMAKE_CROSSOPTS} \
		.
	make -C build
}

check() {
        CTEST_OUTPUT_ON_FAILURE=TRUE make -C build test
}

package() {
        DESTDIR="$pkgdir" make -C build install
}

sha512sums="9975cffec8aa47bcb8c22405bea544cd263dc939b253e8fba2fbaa3b90a60e05b98eaf58365db8ae854d1fff8e641866a7e2e027003ae03a77104d6650dafb6c  fontforge-20220308.tar.xz"
