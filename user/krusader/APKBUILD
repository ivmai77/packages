# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=krusader
pkgver=2.7.2
pkgrel=0
pkgdesc="Advanced, twin-panel (commander style) file manager"
url="https://krusader.org/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev karchive-dev kcodecs-dev
	kbookmarks-dev kcompletion-dev kcoreaddons-dev kconfig-dev kdoctools-dev
	ki18n-dev kiconthemes-dev kitemviews-dev kio-dev knotifications-dev
	kparts-dev solid-dev ktextwidgets-dev kwallet-dev kwidgetsaddons-dev
	kwindowsystem-dev kxmlgui-dev kguiaddons-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/krusader/$pkgver/krusader-$pkgver.tar.xz
	posix-headers.patch"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="8af90a9067c7c92abc9dcc866e1eb1e1f1bd2a4c45968c260b6b01cc079d3125d6db6d49b530d90a2f91d3c99c577fa7f1ff47d12abc3400a75e2d52e7ae477b  krusader-2.7.2.tar.xz
2114547837002e76fea1ed68bd47c8db216a7ba292817b2448497c45e8693441750a5e06ce24de188d0f04dc9c765c2cb5b83b04488a0a5247a481084f6091c0  posix-headers.patch"
