# Contributor: Síle Ekaterin Liszka <sheila@vulpine.house>
# Maintainer: Síle Ekaterin Liszka <sheila@vulpine.house>
pkgname=py3-pycairo
pkgver=1.23.0
pkgrel=0
pkgdesc="Python bindings for Cairo"
url="https://pycairo.readthedocs.io/"
arch="all"
options="!check"  # Test suite requires unpackaged py3-flake8.
license="MIT AND (LGPL-2.1-only OR MPL-1.1)"
depends=""
checkdepends="py3-pytest"
makedepends="python3-dev cairo-dev meson"
subpackages="$pkgname-dev"
source="https://files.pythonhosted.org/packages/source/p/pycairo/pycairo-$pkgver.tar.gz"
builddir="$srcdir/pycairo-$pkgver"

build() {
	meson \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--buildtype=release \
		. output
	ninja -C output
}

check() {
	ninja -C output test
}

package() {
	DESTDIR="$pkgdir" ninja -C output install
}

sha512sums="fc0cc878cc716a6a2baf89a193facfcd89970c884afd3bb9d7fbecf42894ddd191988a8f0fe79100786509f17fbe4f1990eca33a5299414b2df2539d66de5abf  pycairo-1.23.0.tar.gz"
