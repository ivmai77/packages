# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=yakuake
pkgver=22.04.2
pkgrel=0
pkgdesc="Drop-down KDE terminal emulator"
url="https://www.kde.org/applications/system/yakuake/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev karchive-dev kconfig-dev
	kcoreaddons-dev kcrash-dev kdbusaddons-dev kglobalaccel-dev ki18n-dev
	kiconthemes-dev kio-dev knewstuff-dev knotifications-dev kparts-dev
	knotifyconfig-dev kwayland-dev kwidgetsaddons-dev kwindowsystem-dev"
subpackages="$pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/yakuake-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="f4048905ee11031987cd9cc0ae9a1a2a7e1bbf596199ac2092cea06118ea61f3735f36cb8c2a4b1e34dd593c3c320c6a66438bb7d2ee9c63c6bf1f2bed32c7ec  yakuake-22.04.2.tar.xz"
