# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kpmcore
pkgver=22.04.2
pkgrel=0
pkgdesc="Core routines for KDE Partition Manager"
url="https://www.kde.org/applications/system/partitionmanager"
arch="all"
license="LGPL-2.1+"
depends=""
depends_dev="libatasmart-dev parted-dev qt5-qtbase-dev"
makedepends="$depends_dev cmake extra-cmake-modules kcoreaddons-dev ki18n-dev
	kwidgetsaddons-dev util-linux-dev kauth-dev qca-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kpmcore-$pkgver.tar.xz
	filesystem.patch
	test.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="3ce043daccbe5a2d56f6fd849a3b8c6518b49c4c47db1989f7cea60282e63f430047815f6ff7c1a9a4195d9b06d2ad3ec357bc3ed66fef80b253762aea099f15  kpmcore-22.04.2.tar.xz
f8553feb7b73ef3f8e4212b5831354d81c036406ed867805b125a3b1ee24e8df7e524c530d4fb3c4131246342c6ec272dfff074473d8e72e5ff407096806527b  filesystem.patch
547803c36e333a39ad3500e3896cfb407ba8dd15cf08f329c748979c42e3f33a4b1940a11c02d532d00414d6d8f7fa564bb78df986ab9ebf0c62d63a64b4edc7  test.patch"
