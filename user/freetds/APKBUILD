# Contributor: Michael Mason <ms13sp@gmail.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=freetds
pkgver=1.1.42
pkgrel=0
pkgdesc="Library for accessing Sybase and MS SQL Server databases"
url="https://www.freetds.org/"
arch="all"
license="LGPL-2.0+"
depends=""
makedepends="linux-headers openssl-dev unixodbc-dev"
subpackages="$pkgname-doc $pkgname-dev"
source="ftp://ftp.freetds.org/pub/freetds/stable/$pkgname-$pkgver.tar.gz
	fix-includes.patch"

# secfixes:
#   1.1.40-r0:
#     - CVE-2019-13508

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--enable-msdblib \
		--with-openssl=/usr \
		--enable-odbc \
		--with-unixodbc=/usr
	make
}

check() {
	make -C "src/replacements" check
}

package() {
	make -j1 DESTDIR="$pkgdir" install
}

sha512sums="af00e73e761efdd54511bbf98a4d4bd3929658eb56a5749f6266c55dd0ae1036e371ada95eee10705786de1c710e38f7550e9df0e45f50c086945a0df21121d7  freetds-1.1.42.tar.gz
d75d1aab6687586697f3e430db1e82f21208f10076b45996542eea682e36cbbbb344f479a9336fcfd294b5b87d7acb2ec5fb8ddd1914e990e23dd5e7ae93a0b6  fix-includes.patch"
