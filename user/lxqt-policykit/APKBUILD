# Contributor: Síle Ekaterin Liszka <sheila@vulpine.house>
# Maintainer: Síle Ekaterin Liszka <sheila@vulpine.house>
pkgname=lxqt-policykit
pkgver=1.2.0
_lxqt=0.12.0
pkgrel=0
pkgdesc="PolicyKit agent for LXQT"
url="https://lxqt.github.io/"
arch="all"
options="!check"  # No test suite.
license="LGPL-2.1+"
depends=""
makedepends="cmake extra-cmake-modules qt5-qttools-dev polkit-qt-1-dev
	liblxqt-dev>=${pkgver%.*}.0 lxqt-build-tools>=$_lxqt qt5-qttools-dev
	kwindowsystem-dev"
subpackages="$pkgname-doc"
source="https://github.com/lxqt/lxqt-policykit/releases/download/$pkgver/lxqt-policykit-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} -Bbuild
	make -C build
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE make -C build test
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="ba11f0cae52ad3f2e5db55e040bb02f11841d60d3497e483329b81a3cdc8caae8e9e6df08d702314ff9715f7919db2b3cfe82773dccd2d9e385a7b9ab77cf67f  lxqt-policykit-1.2.0.tar.xz"
