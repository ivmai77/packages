# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: 
pkgname=snappy
pkgver=1.1.8
pkgrel=0
pkgdesc="Fast compression and decompression library"
url="https://google.github.io/snappy/"
arch="all"
[ "$CARCH" = "armhf" ] && options="!check"     # does not pass testsuite on armhf
license="BSD-3-Clause"
depends=""
makedepends="cmake"
subpackages="$pkgname-dbg $pkgname-dev"
source="snappy-$pkgver.tar.gz::https://github.com/google/snappy/archive/$pkgver.tar.gz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="efe18ff1b3edda1b4b6cefcbc6da8119c05d63afdbf7a784f3490353c74dced76baed7b5f1aa34b99899729192b9d657c33c76de4b507a51553fa8001ae75c1c  snappy-1.1.8.tar.gz"
