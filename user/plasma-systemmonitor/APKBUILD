# Maintainer: A. Wilcox <awilfox-kde@adelielinux.org>
pkgname=plasma-systemmonitor
pkgver=5.24.5
pkgrel=0
pkgdesc="System monitor tool for KDE Plasma"
url="https://www.kde.org/plasma-desktop/"
arch="all"
license="LGPL-2.0+"
depends="kirigami2 ksystemstats qt5-qtquickcontrols2"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtdeclarative-dev
	kconfig-dev kdbusaddons-dev kdeclarative-dev kglobalaccel-dev ki18n-dev
	kio-dev kitemmodels-dev knewstuff-dev kservice-dev kirigami2-dev
	libksysguard-dev qt5-qtquickcontrols2-dev"
subpackages="$pkgname-lang"
source="https://download.kde.org/stable/plasma/$pkgver/$pkgname-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="b8c4a959eb587b25d8d503b3b2d05e6c8ced3b0cb146e08f0351f424962d059b70c8dd323d55b31ef903157b71bad6f20f969840a860ed25ebdafeb3db18489d  plasma-systemmonitor-5.24.5.tar.xz"
