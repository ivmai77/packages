# Contributor: Síle Ekaterin Liszka <sheila@vulpine.house>
# Maintainer: Síle Ekaterin Liszka <sheila@vulpine.house>
pkgname=libfm-qt
pkgver=1.2.1
_lxqt=0.12.0
pkgrel=0
pkgdesc="Qt library for file management and bindings for libfm"
url="https://lxqt.github.io/"
arch="all"
options="!check"  # No test suite.
license="LGPL-2.1+"
depends=""
depends_dev="libfm-dev menu-cache-dev libexif-dev"
makedepends="cmake extra-cmake-modules lxqt-build-tools>=$_lxqt qt5-qttools-dev
	qt5-qtx11extras-dev $depends_dev"
subpackages="$pkgname-dev"
source="https://github.com/lxqt/libfm-qt/releases/download/$pkgver/libfm-qt-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS -O0" \
		-DCMAKE_C_FLAGS="$CFLAGS -O0" \
		-DPULL_TRANSLATIONS=False \
		${CMAKE_CROSSOPTS} -Bbuild
	make -C build
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE make -C build test
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="2adf82f9b03215013ee793af28019f6a822baaa261c7fe0ac4efe7d2dfdcdb9d232b8380e5a9e4f609c99b8d20326ab065999185f2765fe8c1cdc8513c9978a4  libfm-qt-1.2.1.tar.xz"
