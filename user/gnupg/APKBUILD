# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=gnupg
pkgver=2.2.39
pkgrel=0
pkgdesc="Complete and free implementation of the OpenPGP standard"
url="https://www.gnupg.org/"
arch="all"
license="GPL-3.0+ AND GPL-2.0+ AND LGPL-2.1+ AND LGPL-3.0+ AND MIT AND BSD-3-Clause AND Public-Domain"
depends="pinentry"
makedepends="bzip2-dev gnutls-dev libassuan-dev libgcrypt-dev libgpg-error-dev
	libksba-dev libusb-dev npth-dev openldap-dev sqlite-dev zlib-dev"
pkggroups="gnupg"
install="$pkgname.pre-install $pkgname.pre-upgrade"
subpackages="$pkgname-doc $pkgname-lang"
source="https://gnupg.org/ftp/gcrypt/$pkgname/$pkgname-$pkgver.tar.bz2
	0001-Include-sys-select.h-for-FD_SETSIZE.patch
	t5993-d556-disallow-compressed.patch

	60-scdaemon.rules
	"

# secfixes:
#   2.2.23-r0:
#     - CVE-2020-25125
#   2.2.19-r0:
#     - CVE-2019-14855
#   2.2.17-r0:
#     - CVE-2019-13050

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--enable-nls \
		--enable-bzip2 \
		--enable-tofu \
		--enable-scdaemon \
		--enable-ccid-driver
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install

	mkdir -p "$pkgdir"/lib/udev/rules.d
	install -Dm644 "$srcdir"/60-scdaemon.rules "$pkgdir"/lib/udev/rules.d
}

sha512sums="73f881c12c82010aeaada500517ff39ab22b27ff21b1248bc2228b60a2d75385a44a53c5cfadb8f6b84ef22ad9db0105096b6620fb689560809b324019713940  gnupg-2.2.39.tar.bz2
c6cc4595081c5b025913fa3ebecf0dff87a84f3c669e3fef106e4fa040f1d4314ee52dd4c0e0002b213034fb0810221cfdd0033eae5349b6e3978f05d08bcac7  0001-Include-sys-select.h-for-FD_SETSIZE.patch
47c61274650cebe55ffbd42fd5346afd04c6681a09cd9f51ccb0d253780eb23fd9424afa109426da49d6ea83cd911f6bc50d1f72abd887473ab41c88c25189df  t5993-d556-disallow-compressed.patch
4bfb9742279c2d1c872d63cd4bcb01f6a2a13d94618eff954d3a37451fa870a9bb29687330854ee47e8876d6e60dc81cb2569c3931beaefacda33db23c464402  60-scdaemon.rules"
