# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=partitionmanager
pkgver=22.04.2
pkgrel=0
pkgdesc="Qt-based partition manager"
url="https://www.kde.org/applications/system/partitionmanager"
arch="all"
license="GPL-3.0+"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev kconfig-dev
	kconfigwidgets-dev kcoreaddons-dev kcrash-dev kdoctools-dev ki18n-dev
	kiconthemes-dev kio-dev kjobwidgets-dev kservice-dev kwidgetsaddons-dev
	kxmlgui-dev kpmcore-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/partitionmanager-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="09715df9012b5dab83ce635ca1d2e90e13455a1b798fc1c0a260dbfbc85a48d41db2a96652abc4b8d97a0128c292d360208ce0a6763a4ba5c19963dffc16af42  partitionmanager-22.04.2.tar.xz"
