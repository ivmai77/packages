# Contributor: Johannes Matheis <jomat+alpinebuild@jmt.gr>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=i3lock
pkgver=2.12
pkgrel=0
pkgdesc="Simple X11 screen locker"
url="https://i3wm.org/i3lock/"
arch="all"
options="!check"  # No test suite.
license="MIT"
depends="xkeyboard-config"
makedepends="cairo-dev libev-dev libxkbcommon-dev linux-pam-dev cmd:which
	xcb-util-image-dev xcb-util-xrm-dev"
subpackages="$pkgname-doc"
source="$url/$pkgname-$pkgver.tar.bz2"

prepare() {
	default_prepare
	# At present, this has no functional difference.
	# But when we start developing our PAM stack into something more
	# mature, this will be important, because i3lock runs unprivileged.
	sed -i -e 's:login:base-auth:g' pam/i3lock
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var

	make
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="84f1558368381bcad9a64f41ab6134a6614dea453d1ee5ecfe886185b9e1baebeeca446c4635158deb8dae5b25c09d47d3990239d76c44e5325ca5bfaad9b2ad  i3lock-2.12.tar.bz2"
