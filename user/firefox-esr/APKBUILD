# Contributor: Molly Miller <adelie@m-squa.red>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=firefox-esr
pkgver=91.13.0
pkgrel=2
pkgdesc="Firefox web browser (extended support release)"
url="https://www.mozilla.org/firefox/"
arch="all !ppc !armv7"  # #837, #1015
options="!check"  # Tests disabled
license="MPL-2.0"
depends=""
# moz build system stuff
# python deps
# system-libs
# actual deps
makedepends="
	autoconf2.13 cargo cbindgen clang llvm14-dev node ncurses-dev 
	perl python3 rust rust-std cmd:which

	ncurses-dev openssl-dev

	alsa-lib-dev bzip2-dev icu-dev libevent-dev libffi-dev libpng-dev
	libjpeg-turbo-dev nspr-dev nss-dev pulseaudio-dev zlib-dev

	dbus-glib-dev fts-dev gconf-dev gtk+3.0-dev hunspell-dev
	libnotify-dev libsm-dev libxcomposite-dev libxdamage-dev
	libxrender-dev libxt-dev nasm nss-static sqlite-dev
	startup-notification-dev unzip yasm zip gtk+2.0-dev
	"
_ffxver="$pkgver"'esr'
source="https://ftp.mozilla.org/pub/firefox/releases/$_ffxver/source/firefox-$_ffxver.source.tar.xz
	mozconfig

	bad-google-code.patch
	fix-mutex-build.patch
	fix-seccomp-bpf.patch
	gcc89074.patch
	jpeg-link.patch
	mozilla-build-arm.patch
	newer-cbindgen.patch
	pmmx-double-format.patch
	ppc32-fix.patch
	python3.11-open-U.patch
	python3.11-regex-inline-flags.patch
	rust-32bit.patch
	shut-up-warning.patch
	skia-unified.patch
	stackwalk-x86-ppc.patch
	triplet-vendor-support.patch
	webrender.patch

	firefox.desktop
	firefox-safe.desktop
	"
builddir="$srcdir/firefox-$pkgver"
_mozappdir=/usr/lib/firefox
ldpath="$_mozappdir"

# secfixes: firefox-esr
#   68.0.2-r0:
#     - CVE-2019-11733

prepare() {
	default_prepare
	cp "$srcdir"/mozconfig "$builddir"/mozconfig
	echo "ac_add_options --enable-optimize=\"$CFLAGS\"" >> "$builddir"/mozconfig
	echo "ac_add_options --host=\"$CHOST\"" >> "$builddir"/mozconfig
	echo "ac_add_options --target=\"$CTARGET\"" >> "$builddir"/mozconfig
	echo "mk_add_options MOZ_MAKE_FLAGS=\"-j$JOBS\"" >> "$builddir"/mozconfig

	case "$CARCH" in
		pmmx|x86*)
			echo "ac_add_options --disable-elf-hack" >> "$builddir"/mozconfig
			;;
		ppc)
			export LDFLAGS="$LDFLAGS -latomic"
			;;
		s390x)
			echo "ac_add_options --disable-startupcache" >> "$builddir"/mozconfig
			;;
	esac
}

build() {
	export SHELL=/bin/sh
	export BUILD_OFFICIAL=1
	export MOZILLA_OFFICIAL=1
	export USE_SHORT_LIBNAME=1
	export MACH_USE_SYSTEM_PYTHON=1
	# gcc 6
	export CXXFLAGS="-fno-delete-null-pointer-checks -fno-schedule-insns2"

	case "${CARCH}" in
		armv7)
			# 32-bit memory ceiling, #1012
			LDFLAGS="${LDFLAGS} -Wl,--strip-debug";
			;;
	esac

	# set rpath so linker finds the libs
	export LDFLAGS="$LDFLAGS -Wl,-rpath,${_mozappdir}"

	export UNIXCONFDIR="$srcdir"

	./mach build
}

run() {
	cd "$builddir"/obj-$CHOST/dist/bin
	export LD_LIBRARY_PATH=.
	export PROFILE_DIR="$builddir"/obj-$CHOST/tmp/profile-default
	[ -d $PROFILE_DIR ] || ./firefox -no-remote -CreateProfile "Test $PROFILE_DIR"
	./firefox -no-remote -profile "$builddir"/obj-$CHOST/tmp/profile-default
}

package() {
	export MACH_USE_SYSTEM_PYTHON=1
	DESTDIR="$pkgdir" ./mach install

	install -m755 -d ${pkgdir}/usr/share/applications
	install -m755 -d ${pkgdir}/usr/share/pixmaps

	for png in browser/branding/official/default*.png; do
		local i="${_png%.png}"
		i=${i##*/default}
		install -D -m644 "$png" \
			"$pkgdir"/usr/share/icons/hicolor/${i}x${i}/apps/firefox.png
	done

	install -m644 "$builddir"/browser/branding/official/default48.png \
		${pkgdir}/usr/share/pixmaps/firefox.png
	install -m644 ${startdir}/firefox.desktop \
		${pkgdir}/usr/share/applications/firefox.desktop
	install -m644 ${startdir}/firefox-safe.desktop \
		${pkgdir}/usr/share/applications/firefox-safe.desktop

	# install our vendor prefs
	install -d "$pkgdir"/$_mozappdir/browser/defaults/preferences

	cat >> "$pkgdir"/$_mozappdir/browser/defaults/preferences/firefox-branding.js <<- EOF
	// Use LANG environment variable to choose locale
	pref("intl.locale.matchOS", true);

	// Disable default browser checking.
	pref("browser.shell.checkDefaultBrowser", false);

	// Don't disable our bundled extensions in the application directory
	pref("extensions.autoDisableScopes", 11);
	pref("extensions.shownSelectionUI", true);
	EOF
}

sha512sums="38b4cc52de21e76d6061e6ba175e1cbfd888a16070aa951f5a44283f2db9d7e94f2504621f0da78feac6e71491a6d0e7038f63dd0ae112dcad700eb02e9aa516  firefox-91.13.0esr.source.tar.xz
86168a5e5b8aa3ea7cc0d40174baa97595c82b8e599937155fbd7761e7df7220980c74f75e96c34e5f660423e42ad67761f57b2923389be363253868bac033a9  mozconfig
ace7492f4fb0523c7340fdc09c831906f74fddad93822aff367135538dacd3f56288b907f5a04f53f94c76e722ba0bab73e28d83ec12d3e672554712e6b08613  bad-google-code.patch
c0b2bf43206c2a5154e560ef30189a1062ae856861b39f52ce69002390ff9972d43e387bfd2bf8d2ab3cac621987bc042c8c0a8b4cf90ae05717ca7705271880  fix-mutex-build.patch
70863b985427b9653ce5e28d6064f078fb6d4ccf43dd1b68e72f97f44868fc0ce063161c39a4e77a0a1a207b7365d5dc7a7ca5e68c726825eba814f2b93e2f5d  fix-seccomp-bpf.patch
6eb7fb134760f5d232710c56f18f14de4f533e41e269531edd01f5650f6d641513e34a8d2294af5ad6fd184736f674c734efb4cc003636a75e14a8fdba2fe3b0  gcc89074.patch
240d6c9290a20e2c9ccfa2d15c9a636fc1b09b62f09285eb688974d888390da2a84271f14c397570b58ccdcf29b99370f7183cc0dbbbe581bf6e7d7ee8dcabd2  jpeg-link.patch
e61664bc93eadce5016a06a4d0684b34a05074f1815e88ef2613380d7b369c6fd305fb34f83b5eb18b9e3138273ea8ddcfdcb1084fdcaa922a1e5b30146a3b18  mozilla-build-arm.patch
eb158bf2e4b9d513ae36f3c977a3b110ea8c4801c3c94841bc3ad4cdca3bdfc96d4a662e5a2e662fe7a01b0f1af097280097b16f5d0e83d29b4a7e6cbb8c7396  newer-cbindgen.patch
573b90c73512d2b9971e466b138ffeea94b28f9a20e11e75ee1121e3f6a14fb4bf1a30622da6d48f67e5ea0bc58b200ba5fc3e930bae52f83b2f25f6c10806dd  pmmx-double-format.patch
06a3f4ee6d3726adf3460952fcbaaf24bb15ef8d15b3357fdd1766c7a62b00bd53a1e943b5df7f4e1a69f4fae0d44b64fae1e027d7812499c77894975969ea10  ppc32-fix.patch
07c311921e50fdb86c587000418306292548117f32a69b78db9d8ebd3a1b08196774a3d284b7e2898a2f1fbb9987b2367092f33957824514fd3d7c7b618a83e2  python3.11-open-U.patch
c9c5610b99e73a1eedd3510e73921cba84f8c6d0c58fc7fe5b4a7ec261bd5fe530560856fba46d6b37f84c6e467d5b43946968dc8230b5491bba976b0b5ae33c  python3.11-regex-inline-flags.patch
153f955169e1489d49867be90f68e8a4722cad8edb3a9d1ddb6161e34399e1b4e9e82dc8d72aaba1d5f585bca7c4852433e8ecb068be1583df7155c7cce0223b  rust-32bit.patch
39ddb15d1453a8412275c36fc8db3befc69dffd4a362e932d280fb7fd1190db595a2af9b468ee49e0714f5e9df6e48eb5794122a64fa9f30d689de8693acbb15  shut-up-warning.patch
961fa1c856e97e4d08da4682f520ecf23075571a532a781c5e14dbec4915130b02a8199caf6602013ea904d347c4f06d086b0fe84a3850dd6910d351232da599  skia-unified.patch
452b47b825294779f98ed46bc1065dad76b79ff453521ef049934a120f349c84a1c863b16af1828fe053059823da9690ec917c055ae02dcc5c80c54cad732448  stackwalk-x86-ppc.patch
60ffc4b95ba72aa19fb4f4aaf91393e8c730dae536a19248e2dd21c38cc32891bff69a6b51ea903f185ecc680dae4b21ec11d8cac67b3b038b3f0e757639ad94  triplet-vendor-support.patch
b7c1ac21cd03b7cdc887e005ed970cf13ff95643c7651decf1e6d42094cda6a0464dc2ba3cded3827f6d0f3682c2c9b081a7667f386133aa6e3072d0464e72e8  webrender.patch
f3b7c3e804ce04731012a46cb9e9a6b0769e3772aef9c0a4a8c7520b030fdf6cd703d5e9ff49275f14b7d738fe82a0a4fde3bc3219dff7225d5db0e274987454  firefox.desktop
5dcb6288d0444a8a471d669bbaf61cdb1433663eff38b72ee5e980843f5fc07d0d60c91627a2c1159215d0ad77ae3f115dcc5fdfe87e64ca704b641aceaa44ed  firefox-safe.desktop"
