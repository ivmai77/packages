# Contributor: Brandon Bergren <git@bdragon.rtk0.net>
# Maintainer: Zach van Rijn <me@zv.io>
pkgname=distcc
pkgver=3.4
pkgrel=0
pkgdesc="Distributed builds for C, C++ and Objective C"
url="https://distcc.github.io/"
arch="all"
# BadLogFile_Case depends on non-root, because distcc will NOT drop privs
# and this test checks to ensure that distcc will abort if it can't access
# its log file. Also, distccd WILL privdrop to 'distcc' and will abort if the
# user does not exist. But if we don't run tests as root, we don't need the 
# user on the build box.
options="!checkroot"
license="GPL-2.0+"
depends="python3"
makedepends="binutils-dev cmd:which popt-dev python3-dev"
subpackages="$pkgname-doc"
# These are needed if attempting to use checkroot.
#pkgusers="distcc" # distccd privdrop, see src/setuid.c
#pkggroups="distcc"
install="$pkgname.pre-install"
source="https://github.com/distcc/$pkgname/releases/download/v$pkgver/$pkgname-$pkgver.tar.gz
	heap.patch
	"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--without-avahi \
		--disable-Werror
	make
}

check() {
	# Tests rely on invoking compiler, etc, with a normal path.
	# The default "make check" target gets confused, so we manually
	# invoke the test framework.
	make check_programs
	make PATH="$builddir:/usr/local/bin:/bin:/usr/bin" \
		TESTDISTCC_OPTS="" \
		maintainer-check-no-set-path
}

package() {
	make install DESTDIR="$pkgdir"
}

sha512sums="de09329fdfa25e08a9b9529190ddaa9ceccb34c8655692edb86f367a8db4a71b750c6e928cb8e5a670f51fbbc02fd1c8524f72e01b3ebaacc1106dc676d18eef  distcc-3.4.tar.gz
897429ec358ee36faf7dd19329978d9ba3ac83008158a1df9df8e7c6bc55843b5a681884b67006c526875b26993b64de6fb33e2f7dfac8a2066eabde1f13c348  heap.patch"
