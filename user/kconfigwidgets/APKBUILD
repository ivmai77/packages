# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kconfigwidgets
pkgver=5.94.0
pkgrel=0
pkgdesc="Framework providing widgets for software configuration"
url="https://www.kde.org/"
arch="all"
options="!check"  # All tests require X11
license="LGPL-2.1-only AND LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev kauth-dev kcodecs-dev kconfig-dev kguiaddons-dev
	ki18n-dev kwidgetsaddons-dev kdoctools-dev"
# Doc building requires the following docs:
docdepends="kcodecs-doc kwidgetsaddons-doc kconfig-doc kcoreaddons-doc
	kauth-doc"
makedepends="$depends_dev cmake extra-cmake-modules doxygen qt5-qttools-dev
	$docdepends"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kconfigwidgets-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="1b99bb215d6f00204e6605cc078062d3e6d9b8c3e5b4a2166e82aaf1a50ac02bbede955908bb7c3bd04e6d1733b103f4e50850358d462d3568bd59f8cf43d02e  kconfigwidgets-5.94.0.tar.xz"
