# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=phonon-vlc
pkgver=0.11.1
pkgrel=0
pkgdesc="Phonon backend utilising VLC for media playback"
url="https://www.kde.org/"
arch="all"
license="LGPL-2.1+"
depends="vlc"
makedepends="cmake extra-cmake-modules phonon-dev qt5-qtbase-dev
	qt5-qttools-dev vlc-dev"
subpackages="$pkgname-lang"
source="https://download.kde.org/stable/phonon/phonon-backend-vlc/$pkgver/phonon-backend-vlc-$pkgver.tar.xz
	ecm.patch
	"
builddir="$srcdir/phonon-backend-vlc-$pkgver"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DPHONON_BUILD_PHONON4QT5=True \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="4a068478eb1467477cf5c21813723779742eb15766aee2df8184da8c0b58598c5eff8aecefdcfed8e6136495f3caf7691c99d43e98226ab477c162ff19e1fa4e  phonon-backend-vlc-0.11.1.tar.xz
cfbbe25bd2225754a616dccd9f009027880da8d795dfe43a691d95710069af12a6729cd799f75ff66e01369bb96c31d83eb4d4408743f9a06d35e6daa4233ed0  ecm.patch"
