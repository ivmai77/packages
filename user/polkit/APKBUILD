# Contributor: Carlo Landmeter <clandmeter@gmail.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=polkit
pkgver=0.116
pkgrel=1
pkgdesc="Toolkit for controlling system-wide privileges"
url="https://www.freedesktop.org/wiki/Software/polkit/"
arch="all"
options="!check suid"  # Requires running ConsoleKit and PolKit for JS backend
license="LGPL-2.0+"
depends=""
makedepends="glib-dev gobject-introspection-dev gtk-doc intltool linux-pam-dev
	mozjs-dev autoconf automake libtool elogind-dev"
pkgusers="polkitd"
pkggroups="polkitd"
install="$pkgname.pre-install $pkgname.pre-upgrade"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://www.freedesktop.org/software/polkit/releases/polkit-$pkgver.tar.gz
	0001-make-netgroup-support-optional.patch
	fix-consolekit-db-stat.patch
	fix-test-fgetpwent.patch
	polkit-0.115-elogind.patch
	"

# secfixes:
#   0.115-r2:
#     - CVE-2018-19788

prepare() {
	default_prepare
	autoreconf -vif
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--libexecdir=/usr/lib/polkit-1 \
		--localstatedir=/var \
		--disable-static \
		--enable-nls \
		--enable-introspection \
		--enable-man-pages \
		--with-pam-include=base-auth \
		--disable-gtk-doc-html \
		--disable-gtk-doc-pdf \
		--enable-libelogind=yes

	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="b66b01cc2bb4349de70147f41f161f0f6f41e7230b581dfb054058b48969ec57041ab05b51787c749ccfc36aa5f317952d7e7ba337b4f6f6c0a923ed5866c2d5  polkit-0.116.tar.gz
f13a350a040a80b705d28e2ce3fac183409f593dc360879ce1bc9ec85faa7796cf0f4e054098b737fb816369de6c9d598449f6908316484aac99a44a68102ae6  0001-make-netgroup-support-optional.patch
95493ef842b46ce9e724933a5d86083589075fb452435057b8f629643cac7c7eff67a24fd188087987e98057f0130757fad546d0c090767da3d71ebaf8485a24  fix-consolekit-db-stat.patch
966825aded565432f4fda9e54113a773b514ebf7ee7faa83bcb8b97d218ae84a8707d6747bbc3cb8a828638d692fdef34c05038f150ad38e02a29f2c782aba5b  fix-test-fgetpwent.patch
06432fa56788699762c6978484640554f91728a1cb40679eb47b8514b3c7aa23aac5b9c26586eb4d7043a0af1b319bbe7f869d24844d9151317299b74a8e8f7f  polkit-0.115-elogind.patch"
