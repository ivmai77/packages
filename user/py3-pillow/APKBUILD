# Contributor: Max Rees <maxcrees@me.com>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=py3-pillow
_pkgname=Pillow
_p="${_pkgname#?}"
_p="${_pkgname%"$_p"}"
pkgver=9.1.1
pkgrel=0
pkgdesc="A Python Imaging Library"
url="https://pypi.org/project/Pillow"
arch="all"
# Certified net clean
license="HPND"
depends="py3-olefile python3"
makedepends="python3-dev freetype-dev libjpeg-turbo-dev libwebp-dev
	tiff-dev libpng-dev lcms2-dev openjpeg-dev zlib-dev"
subpackages="$pkgname-doc"
_scripts_rev="b24479c"
source="$pkgname-$pkgver.tar.gz::https://files.pythonhosted.org/packages/source/P/$_pkgname/$_pkgname-$pkgver.tar.gz
	https://dev.sick.bike/dist/$pkgname-scripts-$_scripts_rev.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

# secfixes: pillow
#   9.1.1-r0:
#     - CVE-2020-10177
#     - CVE-2020-10378
#     - CVE-2020-10379
#     - CVE-2020-10994
#     - CVE-2020-11538
#     - CVE-2020-35653
#     - CVE-2020-35654
#     - CVE-2020-35655
#     - CVE-2021-23437
#     - CVE-2021-25287
#     - CVE-2021-25288
#     - CVE-2021-25289
#     - CVE-2021-25290
#     - CVE-2021-25291
#     - CVE-2021-25292
#     - CVE-2021-25293
#     - CVE-2021-27921
#     - CVE-2021-27922
#     - CVE-2021-27923
#     - CVE-2021-28675
#     - CVE-2021-28676
#     - CVE-2021-28677
#     - CVE-2021-28678
#     - CVE-2021-34552
#     - CVE-2022-22815
#     - CVE-2022-22816
#     - CVE-2022-22817
#     - CVE-2022-24303
#     - CVE-2022-30595
#   6.2.2-r0:
#     - CVE-2019-19911
#     - CVE-2020-5310
#     - CVE-2020-5311
#     - CVE-2020-5312
#     - CVE-2020-5313

unpack() {
	default_unpack
	mv pillow-scripts-*/Scripts "$builddir/Scripts"
}

build() {
	# zlib resides in lib
	export CFLAGS="$CFLAGS -L/lib"
	python3 setup.py build
}

check() {
	PYTHONPATH="$(find 'build' -name 'lib.*')" python3 selftest.py
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

doc() {
	pkgdesc="Example scripts for $pkgname"
	depends="$pkgname"

	destdir="$subpkgdir/usr/share/doc/$pkgname"
	install -d "$destdir"/Scripts
	install -m 644 "$builddir"/Scripts/* "$destdir"/Scripts/
	install -Dm644 "$builddir"/LICENSE "$subpkgdir/usr/share/licenses/$pkgname/LICENSE"
}

sha512sums="5fbb94cb3046c002e996f61bfbfaca0b30e9e068aa7ab13083cd2ff8c3e9cc18c5aab04ecc3de5a83eb985186f90dacbd42bad1905b0e89f0e34238af54b304e  py3-pillow-9.1.1.tar.gz
c01e83a7cef6653a33f60acbcbc737f0d40ff0dbc792ce0b2ce52f21092d3071845830fa0f64b27a1c5e679c53df57e0ec2e89867ee717f938d4e6f19db77790  py3-pillow-scripts-b24479c.tar.gz"
