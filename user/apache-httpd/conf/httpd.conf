# This is a sample apache-httpd configuration file.
#
# Sensible defaults have been chosen for everyday use. It is suggested
# that you read the documentation before modifying this file.

# This controls what information is sent in the 'Server' HTTP header.
# This is the most secure option. For internal use, you might go into
# more detail. In order from most informative to least, the possible
# values are: Full, OS, Minor, Minimal, Major, Prod.
ServerTokens Prod
# What IP/port to listen on. By default, listens to port 80 on all IPs.
Listen 80

# Only ONE of the following modules should be uncommented to select the
# Multi-Processing Module that Apache should use to handle connections.
# Read More: https://httpd.apache.org/docs/2.4/mpm.html
# Adélie Linux enables mpm_event by default, as it performs better
# than mpm_prefork for most use cases.
LoadModule mpm_event_module /usr/libexec/apache2/mod_mpm_event.so
#LoadModule mpm_prefork_module /usr/libexec/apache2/mod_mpm_prefork.so

# The rest of these modules are required for basic operations.
LoadModule log_config_module /usr/libexec/apache2/mod_log_config.so
LoadModule mime_module /usr/libexec/apache2/mod_mime.so
LoadModule mime_magic_module /usr/libexec/apache2/mod_mime_magic.so
LoadModule autoindex_module /usr/libexec/apache2/mod_autoindex.so
LoadModule authz_core_module /usr/libexec/apache2/mod_authz_core.so
LoadModule dir_module /usr/libexec/apache2/mod_dir.so
LoadModule unixd_module /usr/libexec/apache2/mod_unixd.so
LoadModule deflate_module /usr/libexec/apache2/mod_deflate.so

# This module is optional, but it's necessary for things like automatically
# upgrading connections to HTTPS if clients try to connect on port 80.
LoadModule rewrite_module /usr/libexec/apache2/mod_rewrite.so

# This module is optional, but you need this if you want to proxy connections,
# particularly via HTTP (e.g. for the acmetool proxy example in
# conf.d/proxy.conf).
#LoadModule proxy_module /usr/libexec/apache2/mod_proxy.so
#LoadModule proxy_http_module /usr/libexec/apache2/mod_proxy_http.so

# This module enables per-user directories.
#LoadModule userdir_module /usr/libexec/apache2/mod_userdir.so

# This module is optional; you must have it enabled if you want HTTP/2.
#LoadModule ssl_module /usr/libexec/apache2/mod_ssl.so

# This module is optional; it enables HTTP/2 support.
#LoadModule http2_module /usr/libexec/apache2/mod_http2.so

# Unix daemon configuration.
<IfModule unixd_module>
User apache
Group apache
</IfModule>

# Per-server configuration; these will be the defaults unless modified
# within <VirtualHost> blocks.

# Server contact address. Not visible by default.
ServerAdmin you@example.com

# Whether to include server information in server-generated pages.
# This is the secure default.
ServerSignature Off

# This directive denies access to the entire server filesystem by default.
# You must explicitly permit access in other <Directory> blocks, such as
# in <VirtualHost> blocks. This is the secure default.
<Directory />
AllowOverride none
Require all denied
</Directory>

# Default to index.html or index.htm for directory indexes.
<IfModule dir_module>
DirectoryIndex index.html index.htm
</IfModule>

# This prevents clients from accessing .htpasswd and .htaccess files.
# This is a secure default.
<Files ".ht*">
Require all denied
</Files>

# This is where errors will be logged. This directive is subsumed by
# <VirtualHost> blocks which also use the directive.
ErrorLog /var/log/apache2/error.log

# How verbose the error log is. Possible values, from most to least verbose:
# debug, info, notice, warn, error, crit, alert, emerg
LogLevel Warn

# A sensible default logging format. Like ErrorLog, this directive is
# subsumed by <VirtualHost> blocks which also use it.
<IfModule log_config_module>
LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" combined
CustomLog /var/log/apache2/access.log combined
</IfModule>

# Mapping of file extension to MIME types. This uses the mapping provided
# by Apache.
<IfModule mime_module>
TypesConfig /etc/apache2/mime.types
</IfModule>

# On the other hand, this module allows Apache to inspect the file in order
# to provide a possibly more accurate MIME type.
<IfModule mime_magic_module>
MIMEMagicFile /etc/apache2/magic
</IfModule>

# Include other configurations which may be available. Configuration for
# optional modules will be found here.
IncludeOptional /etc/apache2/conf.d/*.conf

# Site-specific configurations.
IncludeOptional /etc/apache2/sites.d/*.conf
