# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libtasn1
pkgver=4.16.0
pkgrel=0
pkgdesc="Highly portable ASN.1 library"
url="https://www.gnu.org/software/libtasn1/"
arch="all"
license="LGPL-2.1+"
depends=""
makedepends="texinfo"
subpackages="$pkgname-dev $pkgname-doc $pkgname-tools"
source="https://ftp.gnu.org/gnu/$pkgname/$pkgname-$pkgver.tar.gz"

# secfixes:
#   4.14-r0:
#     - CVE-2018-1000654
#   4.13-r0:
#     - CVE-2018-6003
#   4.12-r1:
#     - CVE-2017-10790

build() {
	CFLAGS="-Wno-error=inline" ./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make -j1
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

tools() {
	pkgdesc="Tools for parsing and manipulating ASN.1"
	license="GPL-3.0+"
	mkdir -p "$subpkgdir"/usr
	mv -i "$pkgdir"/usr/bin "$subpkgdir"/usr/
}

sha512sums="b356249535d5d592f9b59de39d21e26dd0f3f00ea47c9cef292cdd878042ea41ecbb7c8d2f02ac5839f5210092fe92a25acd343260ddf644887b031b167c2e71  libtasn1-4.16.0.tar.gz"
