# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=krunner
pkgver=5.94.0
pkgrel=0
pkgdesc="Parallel query system"
url="https://api.kde.org/frameworks/krunner/html/index.html"
arch="all"
options="!check"  # Test suite requires DBus and X11 session.
license="LGPL-2.0+"
depends=""
depends_dev="qt5-qtbase-dev kconfig-dev kservice-dev plasma-framework-dev"
makedepends="$depends_dev cmake extra-cmake-modules python3 doxygen graphviz
	qt5-qttools-dev qt5-qtdeclarative-dev kcoreaddons-dev ki18n-dev kio-dev
	kpackage-dev threadweaver-dev kactivities-dev"
subpackages="$pkgname-dev $pkgname-doc"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/krunner-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="86f2d81e4dbb0d1c716b4d8fc4d0dbda8494fa81bfb305ddc19d7278250fca48c3bc441e7773a27330d8f9888b81e293b5f585921e99ce252541db3ac0467b30  krunner-5.94.0.tar.xz"
