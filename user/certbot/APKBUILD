# Contributor: zlg <zlg+adelie@zlg.space>
# Maintainer:
pkgname=certbot
pkgver=1.0.0
pkgrel=0
pkgdesc="The EFF's reference ACME client"
url="https://certbot.eff.org/"
arch="noarch"
license="Apache-2.0"
depends="python3 py3-acme py3-configargparse py3-mock py3-configobj py3-cryptography
	py3-parsedatetime py3-rfc3339 py3-tz py3-zope-component py3-zope-interface
	coverage"
# define acme deps here and add them to makedepends
# so they get pulled in when bootstrapping.
_depends_acme="python3 py3-cryptography py3-josepy py3-mock py3-openssl py3-requests
	py3-requests-toolbelt py3-rfc3339 py3-six py3-tz"
makedepends="python3-dev $_depends_acme"
subpackages="py3-acme:acme"
source="certbot-$pkgver.tar.gz::https://github.com/certbot/certbot/archive/v$pkgver.tar.gz"
builddir="$srcdir"/$pkgname-$pkgver/$pkgname

build() {
	python3 setup.py build
}

check() {
	python3 setup.py check
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

acme() {
	pkgdesc="ACME protocol implementation for Python"
	depends="$_depends_acme"
	cd "$builddir"/../acme
	python3 setup.py build
	python3 setup.py install --prefix=/usr --root="$subpkgdir"
}

sha512sums="649ddd3d9305e166ef36de4e4e48b1171d5f369d9d60c026ab37a03e7b221cd815e93e69460662e6436b56cec076005582457e9d45499114306ad808a2497912  certbot-1.0.0.tar.gz"
