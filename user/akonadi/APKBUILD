# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=akonadi
pkgver=22.04.2
pkgrel=0
pkgdesc="Libraries and storage system for PIM data"
url="https://community.kde.org/KDE_PIM/Akonadi"
arch="all"
options="!check"  # Test suite requires running D-Bus session.
license="LGPL-2.1+ AND (LGPL-2.1-only OR LGPL-3.0-only)"
depends="qt5-qtbase-postgresql"
depends_dev="qt5-qtbase-dev boost-dev kconfig-dev kconfigwidgets-dev
	kcoreaddons-dev kio-dev kitemmodels-dev libxml2-dev"
makedepends="$depends_dev cmake extra-cmake-modules libxslt-dev qt5-qttools-dev
	libxslt-dev shared-mime-info sqlite-dev kcompletion-dev kcrash-dev
	kdesignerplugin-dev ki18n-dev kiconthemes-dev kitemviews-dev kio-dev
	kwidgetsaddons-dev kwindowsystem-dev kxmlgui-dev postgresql-dev xz-dev
	doxygen postgresql"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/akonadi-$pkgver.tar.xz
	akonadiserverrc
	atomics.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		-DDATABASE_BACKEND=POSTGRES \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install

	mkdir -p "$pkgdir"/usr/share/config/akonadi
	install -m 644 "$srcdir"/akonadiserverrc \
		"$pkgdir"/usr/share/config/akonadi
}

sha512sums="ea06a24ea0d740ba44705a33e7b7a51e99ca4a51efa735e409dd5be5707d0447ce409e8af88ad316a6064e61f381b27a63262a583d097a79cbdb132d6ca13d45  akonadi-22.04.2.tar.xz
b0c333508da8ba5c447827b2bad5f36e3dc72bef8303b1526043b09c75d3055790908ac9cbb871e61319cfd4b405f4662d62d2d347e563c9956f4c8159fca9ab  akonadiserverrc
3d895231b88aedf3ef45112d637d3b90696386ef8e00df61c5204cd054b01a7c4326748b5126f03abe4ff8ba7bd74e0686bee60060fefeb45aefa7744b2a046e  atomics.patch"
