# Contributor: Sergey Lukin <sergej.lukin@gmail.com>
# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Carlo Landmeter <clandmeter@gmail.com>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Dan Theisen <djt@hxx.in>
pkgname=bind
pkgver=9.16.35
_p=${pkgver#*_p}
_ver=${pkgver%_p*}
_major=${pkgver%%.*}
[ "$_p" != "$pkgver" ] && _ver="${_ver}-P$_p"
pkgrel=0
pkgdesc="The ISC DNS server"
url="https://www.isc.org/downloads/bind/"
arch="all"
# NOTE: The tests were not run because they require that
# the IP addresses 10.53.0.1 through 10.53.0.8 are configured
# as alias addresses on the loopback interface.
# To test, run:
#  cd $builddir
#  sudo bin/tests/system/ifconfig.sh up
#  make check
options="!check"
license="ISC AND Apache-2.0 AND OpenSSL AND BSD-2-Clause AND BSD-3-Clause AND BSD-4-Clause"
pkgusers="named"
pkggroups="named"
depends=""
makedepends="bash openssl-dev libcap-dev perl linux-headers bsd-compat-headers libxml2-dev json-c-dev py3-ply libuv-dev"
subpackages="$pkgname-doc $pkgname-dev $pkgname-libs $pkgname-openrc $pkgname-tools"
install="$pkgname.pre-install"
source="https://ftp.isc.org/isc/${pkgname}${_major}/$_ver/$pkgname-$_ver.tar.xz
	bind.so_bsdcompat.patch
	named.initd
	named.confd
	named.conf.authoritative
	named.conf.recursive
	127.zone
	localhost.zone
	named.ca
	"
builddir="$srcdir/$pkgname-$_ver"

# secfixes:
#   9.16.21-r0:
#     - CVE-2021-25214
#     - CVE-2021-25215
#     - CVE-2021-25216
#   9.16.8-r0:
#     - CVE-2020-8620
#     - CVE-2020-8621
#     - CVE-2020-8622
#     - CVE-2020-8623
#     - CVE-2020-8624
#     - CVE-2020-8618
#     - CVE-2020-8619
#   9.14.9-r0:
#     - CVE-2019-6477
#   9.14.7-r0:
#     - CVE-2019-6475
#     - CVE-2019-6476
#   9.14.3:
#     - CVE-2018-5744
#     - CVE-2018-5745
#     - CVE-2019-6465
#     - CVE-2018-5743
#     - CVE-2019-6467
#     - CVE-2019-6471
#   9.13.3:
#     - CVE-2018-5741
#     - CVE-2018-5740
#   9.13.2:
#     - CVE-2018-5738
#   9.12.1_p2-r0:
#     - CVE-2018-5737
#     - CVE-2018-5736
#   9.11.2_p1-r0:
#     - CVE-2017-3145
#   9.11.0_p5-r0:
#     - CVE-2017-3136
#     - CVE-2017-3137
#     - CVE-2017-3138
#   9.10.4_p5-r0:
#     - CVE-2016-9131
#     - CVE-2016-9147
#     - CVE-2016-9444

prepare() {
	default_prepare

	### http://bugs.gentoo.org/show_bug.cgi?id=227333
	export CFLAGS="$CFLAGS -D_GNU_SOURCE"

	# Adjusting PATHs in manpages
	for k in bin/named/named.rst bin/check/named-checkconf.rst bin/rndc/rndc.rst; do
	sed -i "${k}" \
		-e 's:/etc/named.conf:/etc/bind/named.conf:g' \
		-e 's:/etc/rndc.conf:/etc/bind/rndc.conf:g' \
		-e 's:/etc/rndc.key:/etc/bind/rndc.key:g' \
		;
	done
}

build() {
	./configure \
		--build="$CBUILD" \
		--host="$CHOST" \
		--prefix=/usr \
		--sysconfdir=/etc/bind \
		--localstatedir=/var \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--with-openssl=/usr \
		--with-libxml2 \
		--with-libjson \
                --enable-linux-caps \
		--enable-shared \
		--with-libtool
	make -j${JOBS}
}

package() {
	install -d -m0770 -g named -o root "$pkgdir"/var/bind \
		"$pkgdir"/var/bind/sec \
		"$pkgdir"/var/bind/dyn \
		"$pkgdir"/var/run/named

	install -d -m0750 -g named -o root "$pkgdir"/etc/bind \
		"$pkgdir"/var/bind/pri

	make -j1 DESTDIR="$pkgdir" install

	install -Dm755 "$srcdir"/named.initd \
		"$pkgdir"/etc/init.d/named
	install -Dm644 "$srcdir"/named.confd \
		"$pkgdir"/etc/conf.d/named
	install -Dm644 "$srcdir"/named.conf.authoritative \
		"$pkgdir"/etc/bind/named.conf.authoritative
	install -Dm644 "$srcdir"/named.conf.recursive \
		"$pkgdir"/etc/bind/named.conf.recursive
	install -Dm644 "$srcdir"/named.ca \
		"$pkgdir"/var/bind/named.ca
	install -Dm644 "$srcdir"/127.zone \
		"$pkgdir"/var/bind/pri/127.zone
	install -Dm644 "$srcdir"/localhost.zone \
		"$pkgdir"/var/bind/pri/localhost.zone

	cd "$pkgdir"/var/bind
	ln -s named.ca root.cache
}

libs() {
	default_libs
        for dir in lib usr/lib; do
                for file in "$pkgdir"/$dir/lib*-"$pkgver".so; do
                        [ -f "$file" ] || continue
                        mkdir -p "$subpkgdir"/$dir
                        mv "$file" "$subpkgdir"/$dir/
                done
        done
}

tools() {
	pkgdesc="The ISC DNS tools"
	install=""
	depends=""

	mkdir -p "$subpkgdir"/usr/bin
	for i in dig host nslookup delv nsupdate; do
		mv "$pkgdir"/usr/bin/${i} "$subpkgdir"/usr/bin/
	done

	mkdir -p "$subpkgdir"/usr/sbin
	for i in "$pkgdir"/usr/sbin/dnssec-*; do
		mv "$i" "$subpkgdir"/usr/sbin
	done
}

sha512sums="c979e7a9bcea1c9fb1049a2708d8643c71ad2448a195454fcb3dfacf5d874221e95473e140a6944c3fa249f516718416fb67a50e267522d6bcb2915cdb46e6ea  bind-9.16.35.tar.xz
7167dccdb2833643dfdb92994373d2cc087e52ba23b51bd68bd322ff9aca6744f01fa9d8a4b9cd8c4ce471755a85c03ec956ec0d8a1d4fae02124ddbed6841f6  bind.so_bsdcompat.patch
196c0a3b43cf89e8e3547d7fb63a93ff9a3306505658dfd9aa78e6861be6b226580b424dd3dd44b955b2d9f682b1dc62c457f3ac29ce86200ef070140608c015  named.initd
127bdcc0b5079961f0951344bc3fad547450c81aee2149eac8c41a8c0c973ea0ffe3f956684c6fcb735a29c43d2ff48c153b6a71a0f15757819a72c492488ddf  named.confd
d2f61d02d7829af51faf14fbe2bafe8bc90087e6b6697c6275a269ebbddcaa14a234fff5c41da793e945e8ff1de3de0858a40334e0d24289eab98df4bb721ac5  named.conf.authoritative
3aba9763cfaf0880a89fd01202f41406b465547296ce91373eb999ea7719040bc1ac4e47b0de025a8060f693d3d88774a20d09a43fa7ac6aa43989b58b5ee8fe  named.conf.recursive
eed9886717539399518e011ae5eae6335aed4fae019e1def088c5be26bdc896c99c07adf84ee61babafa31d31ff3b028263d1c88d2eee17ecf4c95a9d77d524c  127.zone
340e86472a2c2746fe585c0aa5f079d3a9b46e828c1f53d48026533a169b7f77ded7d0a13d291d6962607bb9481456e6fa69df1834603e7555332615fb998f0b  localhost.zone
053060aad3efee7775f1793f86717cfdc085144d4af435a2c552bd41d50cc2210cb7c5cd32891ef70de1ad58aaa3477fdeac0fe6325068eadb78e30177970ea4  named.ca"
