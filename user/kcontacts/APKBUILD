# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kcontacts
pkgver=5.94.0
pkgrel=0
pkgdesc="Library for working with contact information"
url="https://www.kde.org"
arch="all"
license="LGPL-2.0+"
depends="iso-codes"
depends_dev="qt5-qtbase-dev"
makedepends="$depends_dev cmake extra-cmake-modules kconfig-dev kcoreaddons-dev
	ki18n-dev kcodecs-dev iso-codes-dev doxygen graphviz qt5-qttools-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kcontacts-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	# All tests except addressformattest require X11 running.
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -R "addressformattest"
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="59291b9639ecf0bf3f28cf1b6cf46239381acc4354fc6cf5d9578ab4eaa5bccfa5e1504478f1be8c435b9b57c77f3c53e41bcae12ddc12ec1fa419ed4bf8f0ec  kcontacts-5.94.0.tar.xz"
