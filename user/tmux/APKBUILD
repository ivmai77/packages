# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Zach van Rijn <me@zv.io>
pkgname=tmux
pkgver=3.3a
pkgrel=0
pkgdesc="Tool to control multiple terminals from a single terminal"
url="https://github.com/tmux/tmux/wiki"
arch="all"
license="MIT"
depends="ncurses-terminfo"
makedepends="bsd-compat-headers libevent-dev libutempter-dev ncurses-dev"
subpackages="$pkgname-doc"
source="https://github.com/tmux/tmux/releases/download/$pkgver/$pkgname-$pkgver.tar.gz"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install

	install -Dm644 example_tmux.conf \
		"$pkgdir"/usr/share/doc/$pkgname/examples/$pkgname.conf
	install -m644 "CHANGES" "$pkgdir"/usr/share/doc/$pkgname/
	install -m644 "README" "$pkgdir"/usr/share/doc/$pkgname/
}

sha512sums="29a846df7d93601c42a22f84f606931dc65da1f70b67d351d0425f77ea3affe3e8218b2940d42cd3dadf3cd1aa95032daad3ecb14fbff0f69939d1beae0498c7  tmux-3.3a.tar.gz"
