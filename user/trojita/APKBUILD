# Contributor: Síle Ekaterin Liszka <sheila@vulpine.house>
# Maintainer: Síle Ekaterin Liszka <sheila@vulpine.house>
pkgname=trojita
pkgver=0.7
pkgrel=3
pkgdesc="Qt-based IMAP email client"
url="http://trojita.flaska.net/"
arch="all"
license="(GPL-2.0-only OR GPL-3.0-only) AND (LGPL-2.1 WITH Nokia-Qt-exception-1.1 OR GPL-3.0-only) AND BSD-3-Clause AND GPL-2.0-only AND GPL-2.0+ AND LGPL-2.0-only AND LGPL-2.0+ AND LGPL-2.1+"
depends=""
# Font required: https://github.com/qtwebkit/qtwebkit/issues/933
checkdepends="ttf-liberation"
makedepends="cmake extra-cmake-modules zlib-dev qt5-qtbase-dev qt5-qtwebkit-dev
	qt5-qttools-dev qt5-qtsvg-dev ragel mimetic-dev gpgme-dev
	qtkeychain-dev"
source="https://sourceforge.net/projects/trojita/files/src/trojita-$pkgver.tar.xz
	use-qgpgme.patch
	fix-gpg.patch
	CVE-2019-10734.patch
	CVE-2020-15047.patch
	attachments.patch
	fetch-task.patch
	testfix.patch
	plaintext-upgrade-attack.patch
	pwstorage.patch
	segfault-sorting-mailboxes.patch
	signedness.patch
	statusbar-fix.patch
	deprecated1.patch
	deprecated2.patch
	deprecated3.patch
	find-gpgme.patch
	"

# secfixes:
#   0.7-r1:
#    - CVE-2019-10734
#    - CVE-2020-15047

prepare() {
	default_prepare
	sed -e "s/--dirty//" -i "$builddir"/cmake/TrojitaVersion.cmake
}

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS -Wno-deprecated" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DWITH_GPGMEPP=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	# test_Html_formatting: requires X11
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E test_Html_formatting
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="fe4d9316f97d913619f27d24a5023c3d8dd4a6b9fb058651be12c67188f394aa8cbb60c7593e5eb28fc12fc883b76deeeb5f4f631edd255fdec4c5862c9a91c8  trojita-0.7.tar.xz
740c2410d7236d722482f05dd1d2c681e35543620823cb7c1396710081f9de4f6ae530c5b5442ecf5d08a8e552f0697f3a35bf51e07a3b4336dec7021b665706  use-qgpgme.patch
9d0fbf7c0b0f6975990a7705f9d43043e5807401cee179d7a07f9514856332d6bb1aa8448e84d0083003c34a3bb181080b973e8c1f77d1b5a8930d07d57702da  fix-gpg.patch
db96a566924b5d7b80787ab624af3726d5dd3459653192436a377d6482ab73801a7dcca1df1b1d937cf0d0798b827e04f8ef2c1124f91dc9da3e8036ef61e28a  CVE-2019-10734.patch
2477612aca1e558fa3ba2b434a701cc255c573ac7e2001e7b5921c9b991f7c95720f53b70b49824e36bafb53ab53477950cb8d436e637fda4d59c7ec5883ce5f  CVE-2020-15047.patch
714b6d3b8a791783039f51d98b5d14b2b01ec3228993500623c980c09d04c38e6f26d236efa7fc722d68cd72692b646e5c4b2ca13d3d9b342e2072afb466ded0  attachments.patch
af6d1951fe89a1347b0793175e2348c9cf883792bd576cead9cd70b83546d3dd29a59390c167e76d67413f822368d16892f11dac08da2ce0d9ed53e51b2c0d52  fetch-task.patch
762c10e004e51d229f5a88410b049716abe3bd25f7ff904b031c1d6ed4a637449e47e2219c5ac85db36517d7599f48cd8a15a0cc2d87ba9e5cba79d483bf8d84  testfix.patch
1ed2feb300304c58902e034767747610641c998b7a33fede01ff850b298074a04b0aa23332f8b37ecd9d798c35126d0d8f2e7560699c672a2195f110891cbe1a  plaintext-upgrade-attack.patch
86f5dce6bbc7926105bb2950aa15ca469a65cb29d1197d660a3de8ebbe19bb54a945803c542540c168f0b9dc3bfbde58e160bb4f47e0c418fcbb81744f3f60a2  pwstorage.patch
93b8e795ef3f4423f1a1cce4ca87d146252af33259be2ed597af822a5c8b4abca2292055799c5b5ab70983b8dbba667e516dfe28e6c26705266f3f05b275ad4b  segfault-sorting-mailboxes.patch
83fd69307bb6c34c72a566e06674cc63f330d967d50812067c66633d9cfeb450410257042d489d4be591af383725e5d5f465421675065140fab6c6a40ab55484  signedness.patch
b17f19a7c9a02fbf711b6e960bf9679caf591c35b59ec636293d341286135aaef7e6d137076502a2087375d30bf75cd5c8d67bcf726f75b0ab76aae0d8f24927  statusbar-fix.patch
7a7d71e415c2b3e11c962eedf5fe3b5b22579fc86855a904a8d4c1ee2bf300d0872e0a9081644c33b197b2ad5eda9b0209fe7bccfcce466efd9924ea52282fe5  deprecated1.patch
2f1c1de165f8fede77cbb0fe9a5cc1139def4d903692adefcc25a14fcec0c50f1070d458ddaa17bdb2c999f54c3dbf6e6403ccd5c897f4c861a1df7d121e978a  deprecated2.patch
bce7713fb5bef29b2b189d281dee57ddfc57f1c0601f424aba2185668215878b9bfd3bb80f0d2f8b73219e782db5e64973e79190fe62afa8ba98327535139146  deprecated3.patch
51a5d0a0f48cbc88d8eaaf24a3820f6045402dff9e4e1958aae2211c5273798e935667af843e707731de546ba28f66e8cc4588123ffea5a4ad6c7b3ca62055f5  find-gpgme.patch"
