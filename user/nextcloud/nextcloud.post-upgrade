#!/bin/sh

ver_new="$1"
ver_old="$2"

maj_new="${ver_new%%.*}"
maj_old="${ver_old%%.*}"
maj_diff="$((maj_new - maj_old))"

if [ "$(apk version -t "$ver_old" "14.0.13-r0")" = "<" ]; then
	cat >&2 <<-EOF
	*
	* Some of Nextcloud's versions may be out of sync.
	* You can check by running:
	*
	*     apk info -v | grep '^nextcloud-.*-$ver_old'
	EOF
fi

if [ "$maj_diff" -lt 0 ]; then
	cat >&2 <<-EOF
	*
	* Warning: Nextcloud does not support downgrading major versions.
	* Only run "occ upgrade" if you have skipped major versions
	* and are in the middle of fixing that!
	*
	EOF
	exit 0
fi

if [ "$maj_diff" -gt 1 ]; then
	cat >&2 <<-EOF
	*
	* Warning: Nextcloud does not support skipping major versions.
	* Try the following instead:
	*
	EOF

	maj_old="$((maj_old + 1))"
	while [ "$maj_old" -lt "$maj_new" ]; do
		cat >&2 <<-EOF
		*     apk add nextcloud~$maj_old
		*     occ upgrade
		EOF
		maj_old="$((maj_old + 1))"
	done
	cat >&2 <<-EOF
	*     apk add nextcloud
	*     occ upgrade
	*
	EOF
	exit 0
fi

if [ "${ver_new%-r*}" != "${ver_old%-r*}" ]; then
	cat >&2 <<-EOF
	*
	* Run "occ upgrade" to finish upgrade of your Nextcloud instance!
	*
	EOF
fi
