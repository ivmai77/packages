# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kdeclarative
pkgver=5.94.0
pkgrel=0
pkgdesc="Frameworks for creating KDE components using QML"
url="https://www.kde.org/"
arch="all"
options="!check"  # Requires accelerated X11 desktop running
license="LGPL-2.0+"
depends=""
depends_dev="qt5-qtbase-dev qt5-qtdeclarative-dev kiconthemes-dev kio-dev
	knotifications-dev kpackage-dev libepoxy-dev"
docdepends="kcoreaddons-doc kconfig-doc kpackage-doc"
makedepends="$depends_dev cmake extra-cmake-modules python3 doxygen graphviz
	qt5-qttools-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kdeclarative-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="dfe5359a0dcc75e00ef0ece97903b4217e896d3427633e9dbec834cf924d921982576651139e9215b01d283213ce0d73d03ab67b9d3561763ca0596ad80067b4  kdeclarative-5.94.0.tar.xz"
