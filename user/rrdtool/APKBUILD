# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Maintainer: Zach van Rijn <me@zv.io>
pkgname=rrdtool
pkgver=1.8.0
pkgrel=1
pkgdesc="Data logging and graphing application"
url="https://oss.oetiker.ch/rrdtool/"
arch="all"
license="GPL-2.0+"
depends="font-sony-misc"
checkdepends="bc"
makedepends="cairo-dev freetype-dev groff libart-lgpl-dev libpng-dev
	libxml2-dev pango-dev perl-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang perl-rrd:perl_rrd
	$pkgname-cached $pkgname-cgi $pkgname-utils librrd:libs"
source="https://github.com/oetiker/$pkgname-${pkgver%%.*}.x/releases/download/v$pkgver/$pkgname-$pkgver.tar.gz
	rrdcached.initd
	signedness.patch
	time64.patch
	"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--disable-tcl \
		--disable-ruby \
		--enable-rrdcgi \
		--enable-perl-site-install \
		--with-perl-options="INSTALLDIRS=vendor"
	make
}

check() {
	LANG=C.UTF-8 LC_ALL=C.UTF-8 TZ=UTC make check
}

package() {
	export INSTALLDIRS=vendor
	make DESTDIR="$pkgdir" install
	find "$pkgdir" -name '.packlist' -delete
	find "$pkgdir" -name 'perllocal.pod' -delete
}

dev() {
	default_dev
	mkdir -p "$subpkgdir"/usr/share/$pkgname
	mv "$pkgdir"/usr/share/$pkgname/examples "$subpkgdir"/usr/share/$pkgname/
}

perl_rrd() {
	depends=""
	pkgdesc="Perl interface for rrdtool"
	mkdir -p "$subpkgdir"/usr/lib \
		"$pkgdir"/usr/share
	mv "$pkgdir"/usr/lib/perl* "$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/share/perl* "$subpkgdir"/usr/share/
}

cached() {
	depends=""
	pkgdesc="RRDtool data caching daemon"
	mkdir -p "$subpkgdir"/usr/sbin \
		"$subpkgdir"/var/lib/rrdcached/db \
		"$subpkgdir"/var/lib/rrdcached/journal
	mv "$pkgdir"/usr/bin/rrdcached "$subpkgdir"/usr/sbin
	install -Dm755 "$srcdir"/rrdcached.initd "$subpkgdir"/etc/init.d/rrdcached
}

cgi() {
	depends=""
	pkgdesc="Create web pages containing RRD graphs based on templates"
	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/rrdcgi "$subpkgdir"/usr/bin
}

utils() {
	depends=""
	pkgdesc="RRDtool standalone utilities"
	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/rrdinfo \
		"$pkgdir"/usr/bin/rrdcreate \
		"$pkgdir"/usr/bin/rrdupdate \
		"$subpkgdir"/usr/bin
}

sha512sums="8ae6f94d119e8d0e1ba7f2d0738f1ba008a4880d1022f1c0c5436f662d961fceec5c42e01c241493ece3d6f55c60fd7d1d264f93e678f3cf1251201dcde027c1  rrdtool-1.8.0.tar.gz
c0c27b2c2dfa8e7ec1cb1160d2bda8d7996bbea67f4ce7779da029f583c35e5e415cf46e2a1e5cb8ed2e63d2c58a68fd7471ee6bd820db4c0f4eeeb5c252f8a3  rrdcached.initd
da1218051c10c86bf19a1e2435f69dc072f231d22513bc474491bd7901427921be5ba1ff889166e9f9b3a95473055d64dfbc53d477da8b6446de2d5901ff78e6  signedness.patch
aa119ea9a9b819b6d51b3a4351f26a34f0d4fdfeb28ee8f1b20a16ca8b9b1f6a6b85012fc53e40cc6ddee4dc0006b095476363f54e8c856b39da2ed7f4a0b8d9  time64.patch"
