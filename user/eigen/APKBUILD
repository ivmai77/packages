# Contributor: Bradley J Chambers <brad.chambers@gmail.com>
# Maintainer: 
pkgname=eigen
pkgver=3.3.8
pkgrel=1
pkgdesc="Eigen is a C++ template library for linear algebra"
url="http://eigen.tuxfamily.org/index.php?title=Main_Page"
arch="noarch"
options="!check"  # Headers-only: no tests are possible.
license="MPL-2.0"
depends=""
makedepends="cmake"
subpackages="$pkgname-dev"
source="https://gitlab.com/libeigen/eigen/-/archive/$pkgver/eigen-$pkgver.tar.gz
	assert-compile-error.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		-Bbuild
	# Header-only library, so there is no 'build'.
}

package() {
	make -C build DESTDIR="$pkgdir" install
	mkdir -p "$pkgdir"/usr/lib/cmake
	mv "$pkgdir"/usr/share/eigen3/cmake "$pkgdir"/usr/lib/cmake/Eigen3
}

sha512sums="5b4b5985b0294e07b3ed1155720cbbfea322fe9ccad0fc8b0a10060b136a9169a15d5b9cb7a434470cadd45dff0a43049edc20d2e1070005481a120212edc355  eigen-3.3.8.tar.gz
26cdf877d4d3378b322cc08c0430b0628a07279a030732661ba4acb85d6c835956c99474fc4587a170d11437ca6715e56d8eaabb625a1eba7c46ce4d6e4d66f9  assert-compile-error.patch"
