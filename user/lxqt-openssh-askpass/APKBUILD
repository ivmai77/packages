# Contributor: Síle Ekaterin Liszka <sheila@vulpine.house>
# Maintainer: Síle Ekaterin Liszka <sheila@vulpine.house>
pkgname=lxqt-openssh-askpass
pkgver=1.2.0
_lxqt=0.12.0
pkgrel=0
pkgdesc="Graphical LXQt utility for inputting passwords for SSH agents"
url="https://lxqt.github.io/"
arch="all"
options="!check"  # No test suite.
license="LGPL-2.1+"
depends=""
makedepends="cmake extra-cmake-modules qt5-qttools-dev lxqt-build-tools>=$_lxqt
	liblxqt-dev>=${pkgver%.*}.0 kwindowsystem-dev"
subpackages="$pkgname-doc"
source="https://github.com/lxde/lxqt-openssh-askpass/releases/download/$pkgver/lxqt-openssh-askpass-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} -Bbuild
	make -C build
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE make -C build test
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="8093516f6e1616a5e012d9af2c491580ab8a35053d4c47045b95a151e2e0e0c6f4055e2cb009140883b31057911d5fec80d5a1f84be8e15f7470dbc0e348960e  lxqt-openssh-askpass-1.2.0.tar.xz"
