# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=utf8proc
pkgver=2.4.0
pkgrel=0
pkgdesc="C library for processing UTF-8 data"
url="http://juliastrings.github.io/utf8proc"
arch="all"
license="MIT AND Unicode-DFS-2016"
depends=""
makedepends="cmake"
subpackages="$pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/JuliaStrings/utf8proc/archive/v$pkgver.tar.gz
	https://dev.sick.bike/dist/$pkgname-testdata-$pkgver.tar.gz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		-Bbuild
	make -C build
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="2bbd056b488cd30faca26618389d8af84edc39ade9b705e147b676bf39eee65b40239d01c32c46dfc2a289d79e869ed1bb3c347365603dcaab2f69e34427441a  utf8proc-2.4.0.tar.gz
1d41e9138ef2f41a0bf79e0613695c516da87f79f53032476d585f91040f9c6231f4e67944b43ffa16d7d96cfd3841c15d7f09a7178f45ac6b6b3585e97050fc  utf8proc-testdata-2.4.0.tar.gz"
