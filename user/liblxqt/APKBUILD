# Contributor: Síle Ekaterin Liszka <sheila@vulpine.house>
# Maintainer: Síle Ekaterin Liszka <sheila@vulpine.house>
pkgname=liblxqt
pkgver=1.2.0
_lxqt=0.12.0
_qtxdg=3.10.0
pkgrel=0
pkgdesc="Core LXQt library"
url="https://lxqt.github.io/"
arch="all"
options="!check"  # No test suite.
license="LGPL-2.1+"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtx11extras-dev
	qt5-qttools-dev libxscrnsaver-dev libqtxdg-dev>=$_qtxdg
	kwindowsystem-dev polkit-qt-1-dev lxqt-build-tools>=$_lxqt"
subpackages="$pkgname-dev"
source="https://github.com/lxqt/liblxqt/releases/download/$pkgver/liblxqt-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} -Bbuild
	make -C build
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="3e9bc5946107a5486fde1ecea1637d16901a272a229525dc3f9c0450225287ce41e3bbec9441797a24bc850ba16e3e1efe172d871c5a6685b5b228b88b5a2221  liblxqt-1.2.0.tar.xz"
