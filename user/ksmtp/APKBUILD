# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=ksmtp
pkgver=22.04.2
pkgrel=0
pkgdesc="SMTP library for KDE"
url="https://kde.org/"
arch="all"
license="LGPL-2.1+"
depends=""
makedepends="qt5-qtbase-dev cmake extra-cmake-modules kauth-dev kcodecs-dev
	kcoreaddons-dev ki18n-dev kio-dev kjobwidgets-dev kservice-dev
	solid-dev cyrus-sasl-dev"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/ksmtp-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="a7a02c146733bb6755e1c7c342b04b8a4959ebca3ef8ea948e377a3cd206f215dab867654e885bbb02fa8b0746636f38352883da51e68acfffb6252468d8d988  ksmtp-22.04.2.tar.xz"
