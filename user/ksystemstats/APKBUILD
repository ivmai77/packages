# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox-kde@adelielinux.org>
pkgname=ksystemstats
pkgver=5.24.5
pkgrel=0
pkgdesc="KDE system statistics daemon"
url="https://www.kde.org/"
arch="all"
options="!check"  # Requires D-Bus.
license="BSD-2-Clause AND BSD-3-Clause AND CC0-1.0 AND GPL-2.0-only AND GPL-2.0+ AND GPL-3.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev kauth-dev kcodecs-dev
	kcoreaddons-dev kdbusaddons-dev ki18n-dev kio-dev kjobwidgets-dev
	kservice-dev libksysguard-dev libnl3-dev lm_sensors-dev solid-dev
	eudev-dev networkmanager-qt-dev"
subpackages="$pkgname-lang"
source="https://download.kde.org/stable/plasma/$pkgver/ksystemstats-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="6bbcf6512c223fc97203c02baca5bb230ef86c8012318307e32bc030160f31fc2797a28d95af442826b5f48656673213f6cdde487e1782a50300a9bc96e22969  ksystemstats-5.24.5.tar.xz"
