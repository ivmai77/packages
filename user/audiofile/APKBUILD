# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=audiofile
pkgver=0.3.6
pkgrel=1
pkgdesc="Library for reading and writing audio files in many formats"
url="https://audiofile.68k.org"
arch="all"
license="LGPL-2.1+"
depends=""
depends_dev="flac-dev"
makedepends="$depends_dev alsa-lib-dev"
subpackages="$pkgname-dev $pkgname-doc"
source="https://audiofile.68k.org/audiofile-$pkgver.tar.gz
	tests-unsigned-gcc6.patch

	CVE-2015-7747.patch
	CVE-2017-6827,6828,6832,6833,6835,6837.patch
	CVE-2017-6829.patch
	CVE-2017-6830,6834,6836,6838.patch
	CVE-2017-6831.patch
	CVE-2017-6839.patch
	CVE-2018-13440,17095.patch
	"

prepare() {
	default_prepare
	update_config_sub
}

build() {
	export CXXFLAGS="$CXXFLAGS -fpermissive"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="f9a1182d93e405c21eba79c5cc40962347bff13f1b3b732d9a396e3d1675297515188bd6eb43033aaa00e9bde74ff4628c1614462456529cabba464f03c1d5fa  audiofile-0.3.6.tar.gz
b85546eaccf0911fb2f5ba26c323ea2c0b18cae7d7476d5ac9e821c90097f331e3391b787f390d130382fffb6b777887344f914489ca1bc0a3974f1ebe16e328  tests-unsigned-gcc6.patch
7673ab3fafdb0dac514a42622f53ea17aa56836c76413e5680c475537e195c53df21f26da1bd4e7941df2dc8b33a471ab52d539dabffbaef8bc95ee59951e7fe  CVE-2015-7747.patch
880392c89e0f5982ceb3f56b32da7f28217df64dedf588eb2a72616367f77813b34f3f12092d0b05818247da8e411de8fded336fe09413e81184e98546489b3a  CVE-2017-6827,6828,6832,6833,6835,6837.patch
e7afe1a27566fb593ea53176256df23e447a2ee842cb4168930dec365fdabe7f2f43512d81bca5f14336ef0c756f6006c24948a3c2d79baafb0042ed8a145aae  CVE-2017-6829.patch
3b870602fab366015de6dc2f3cf83eae90a4113e2c18e843494c3a75744730f41aefd99547ea758cef194d921a7fc66024b1b9ffbf0bbe6614693453cce09e99  CVE-2017-6830,6834,6836,6838.patch
51c92ce66e987ae1d4bda65247134097705ef45cf7670401af7943bf6bbfc674089bcfafa49983046b10573ea72900adb96c296739c234d5e98539098eebe022  CVE-2017-6831.patch
88603061ffe607910d0c73b88c2a305134d6ea4d51dadb6a33163d24c7dd3ab091e7f32c8549fec442c4898a2d6ba91da209973d002c47342645d82d3957a0f0  CVE-2017-6839.patch
b90684b8e8082acd84f40ec8da83a6f2a2280e71be873055829d4555377454797446f4f5f77c7a9cda7aa4450f1647370a16e2284b5b7777eb86da4ff7d1e336  CVE-2018-13440,17095.patch"
