# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kwallet
pkgver=5.94.0
pkgrel=0
pkgdesc="Secure storage system for passwords built atop Qt"
url="https://www.kde.org/"
arch="all"
license="LGPL-2.1+"
depends=""
depends_dev="qt5-qtbase-dev kcoreaddons-dev kconfig-dev kwindowsystem-dev
	ki18n-dev kconfigwidgets-dev kdbusaddons-dev kiconthemes-dev
	knotifications-dev kservice-dev libgcrypt-dev gpgme-dev"
makedepends="$depends_dev cmake extra-cmake-modules kdoctools-dev doxygen
	qt5-qttools-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kwallet-$pkgver.tar.xz
	kwallet-5.22.0-blowfish-endianness.patch"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DBUILD_QCH:BOOL=ON \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="9cb0105413d983bf89819f60fc36182cbb59737be316ce52262fe68f388dc1262dfb404ef0703af48ec75bd869985829f67b58f433f6b366ef780ad553ee9638  kwallet-5.94.0.tar.xz
5aea35d4b1e706c6712c8e2a2ab4bb243ad0c215e677ac922226bc95602112111dbd9675d42184a88dced60f1f700c8ac18eba60e48f068b4b642e0b6189d78f  kwallet-5.22.0-blowfish-endianness.patch"
