# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=dbus
pkgver=1.12.20
pkgrel=0
pkgdesc="IPC/message bus system, allowing user applications to communicate"
url="https://www.freedesktop.org/wiki/Software/dbus/"
pkggroups="messagebus"
pkgusers="messagebus"
arch="all"
options="!check"  # Requires running X11 server.
license="AFL-2.1 OR GPL-2.0+"
subpackages="$pkgname-dev $pkgname-doc $pkgname-libs $pkgname-openrc
	$pkgname-x11"
triggers="$pkgname.trigger=/etc/dbus-1/system.d"
depends=""
depends_dev="util-linux-dev"
makedepends="$depends_dev expat-dev libx11-dev autoconf automake libtool xmlto
	libsm-dev"
install="$pkgname.pre-install $pkgname.post-install"
source="http://dbus.freedesktop.org/releases/dbus/dbus-$pkgver.tar.gz
	$pkgname.initd
	"

# secfixes:
#   1.12.16-r0:
#     - CVE-2019-12749
#   1.12.18-r0:
#     - CVE-2020-12049

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--localstatedir=/var \
		--with-dbus-user=messagebus \
		--with-system-pid-file=/var/run/dbus.pid \
		--disable-verbose-mode \
		--disable-static \
		--enable-inotify \
		--disable-tests \
		--disable-asserts \
		--enable-user-session \
		--enable-xml-docs \
		--with-session-socket-dir=/tmp \
		--with-x
	make
}

package() {
	make -j1 DESTDIR="$pkgdir" install
	install -Dm755 "$srcdir"/dbus.initd "$pkgdir"/etc/init.d/dbus
}

libs() {
	pkgdesc="Libraries to communicate over D-Bus"
	replaces="dbus"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/lib*.so.* "$subpkgdir"/usr/lib/
}

x11() {
	pkgdesc="X11 add-ons for D-Bus"
	depends="dbus=$pkgver-r$pkgrel"
	mkdir -p "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/dbus-launch "$subpkgdir"/usr/bin/
}

sha512sums="0964683bc6859374cc94e42e1ec0cdb542cca67971c205fcba4352500b6c0891665b0718e7d85eb060c81cb82e3346c313892bc02384da300ddd306c7eef0056  dbus-1.12.20.tar.gz
df74e7d6a4f76f777d356e94bd23422b17656aa51a5b2d3c655fcabb32c84f2f06b9f5cd8827920d51842f89e8c0d968a6e723315e4bf216e55711fcda9b0ee9  dbus.initd"
