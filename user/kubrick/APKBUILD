# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kubrick
pkgver=22.04.2
pkgrel=0
pkgdesc="3D cube puzzle solving game"
url="https://www.kde.org/applications/games/kubrick/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtsvg-dev kconfig-dev
	kconfigwidgets-dev kcoreaddons-dev kcrash-dev kdoctools-dev ki18n-dev
	kio-dev kwidgetsaddons-dev kxmlgui-dev libkdegames-dev glu-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kubrick-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="5f3e5d4e589528348e9754acf7a71689cdd675e628cec235935be7d0f1b3bb7597d02071fae44843124bc0ddac80c5b4b0094337d0cb6a238675885c130e69eb  kubrick-22.04.2.tar.xz"
