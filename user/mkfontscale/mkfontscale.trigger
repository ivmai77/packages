#!/bin/sh

for i in "$@"; do
	mkfontscale "$i"
	mkfontdir -e "/usr/share/fonts/X11/encodings" -e "/usr/share/fonts/X11/encodings/large" -- "$i"
done

