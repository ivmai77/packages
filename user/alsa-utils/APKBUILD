# Contributor: Leonardo Arena <rnalrd@alpinelinux.org>
# Maintainer: Max Rees <maxcrees@me.com>
pkgname=alsa-utils
pkgver=1.2.4
pkgrel=0
pkgdesc="Advanced Linux Sound Architecture (ALSA) utilities"
url="https://www.alsa-project.org/wiki/Main_Page"
arch="all"
options="!check"  # No test suite.
license="GPL-2.0+ AND LGPL-2.0+ AND ISC AND GPL-2.0-only"
depends="bash dialog"
makedepends="alsa-lib-dev fftw-dev ncurses-dev pciutils-dev"
subpackages="$pkgname-doc $pkgname-dbg $pkgname-lang $pkgname-openrc"
replaces="alsaconf"
source="ftp://ftp.alsa-project.org/pub/utils/$pkgname-$pkgver.tar.bz2
	alsaconf.patch
	alsa.initd
	alsa.confd
	"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--disable-xmlto
	make
}

package() {
	make DESTDIR="$pkgdir" install
	install -Dm755 ../alsa.initd "$pkgdir"/etc/init.d/alsa
	install -Dm644 ../alsa.confd "$pkgdir"/etc/conf.d/alsa
}

sha512sums="13080abda55269513b3751044dac292d695e273073a62d74ed4a32c68f448a2b015fe16604650821a6398b6ef6a7b6008cb9f0b7fb7f4ee1fa2b4eb2dcf29770  alsa-utils-1.2.4.tar.bz2
817215be6e9f103a8a187df5b1142c4d2e952f547a64579a9b8cfa58bd762d6a55bde75c0f66f018c0597744d07ccdb08216f7b368db464e36667cecedcc00f3  alsaconf.patch
7bf743024d7c5caed2fbf8af5cee73bcc569a7bab0bd6459541d3704cc6a7456d588b600b690e7406e122deaf0316dd1f67219a267bec4dff3f6c0f120edaae4  alsa.initd
6e716e6230fd3d2c33e3cb2dbf572d632c9ac6452c1768388bea7d3ca22f7c72cf6bcd702580f45cb9089983582011c8b04cbdb4420d14fb988167b1391ea547  alsa.confd"
