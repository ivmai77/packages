# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=abiword
pkgver=3.0.5
pkgrel=0
pkgdesc="A fully-featured word processor"
url="https://www.abisource.com"
arch="all"
options="!check"  # Test suite requires valgrind, etc
license="GPL-2.0+"
depends=""
makedepends="bzip2-dev enchant-dev fribidi-dev goffice-dev gtk+3.0-dev
	libgsf-dev libjpeg-turbo-dev librsvg-dev libxslt-dev pcre-dev
	popt-dev wv-dev"
# openxml plugin
makedepends="$makedepends boost-dev"
# collab plugin
makedepends="$makedepends gnutls-dev libsoup-dev dbus-glib-dev"
subpackages="$pkgname-dev $pkgname-doc"
_plugins="applix babelfish bmp clarisworks collab docbook eml epub \
	freetranslation garble gdict gimp google hancom hrtext iscii kword \
	latex loadbindings mht mif mswrite openwriter openxml opml paint \
	passepartout pdb pdf presentation s5 sdw t602 urldict wikipedia wml \
	xslfo"
# https certificate is expired; integrity provided by checksum
source="http://www.abisource.com/downloads/$pkgname/$pkgver/source/$pkgname-$pkgver.tar.gz
	fix-bad-cast.patch
	"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--enable-shared \
		--disable-static \
		--enable-plugins="$_plugins"
	make
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="a2484268901ff47307c9d1f1928622e364f1006f22ce38257c585144df9411dfe3c2dea28c1f1f50a6e545e8cc579cce34117a89dfa771e20312e3ea1a9989d6  abiword-3.0.5.tar.gz
89edeed246f937e4f198068ed9861e155662d346fb1534d2ed676cbd782de22a691d2b045989942bd715cbfc3750cf9d7615065816b970695597361ae4a9d55e  fix-bad-cast.patch"
