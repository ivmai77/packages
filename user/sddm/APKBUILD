# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=sddm
pkgver=0.19.0
pkgrel=0
pkgdesc="Simple Desktop Display Manager"
url="https://github.com/sddm/sddm/"
pkgusers="sddm"
pkggroups="sddm"
arch="all"
license="GPL-2.0+"
depends="dbus-x11 elogind"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev libxcb-dev upower-dev
	elogind-dev linux-pam-dev qt5-qtdeclarative-dev qt5-qttools-dev
	utmps-dev"
subpackages="$pkgname-lang $pkgname-openrc"
install="sddm.post-install"
langdir="/usr/share/sddm/translations"
# Patch files are in order of needed application.
source="https://github.com/sddm/sddm/releases/download/v$pkgver/sddm-$pkgver.tar.xz
	pam-path-fix.patch
	utmpx.patch
	revert-fedora.patch
	xephyr.patch
	retry.patch
	cleanup-xorg.patch
	authed.patch
	allocate-vt.patch
	autologin-type.patch
	autologin-pam.patch
	session-env.patch
	qt515.patch
	sddm.initd
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=Debug \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		-DENABLE_JOURNALD=OFF \
		-DNO_SYSTEMD=ON \
		-DUSE_ELOGIND=ON \
		-DUID_MIN=500 \
		-DUID_MAX=65000 \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
	install -D -m755 "$srcdir"/sddm.initd "$pkgdir"/etc/init.d/sddm
}

openrc() {
	default_openrc
	depends="sddm xorg-server"
}

sha512sums="0a40816bc105a1e930fec2d65fabff0ae7e27c641235d90e41f6fbaa86af4bb774a9e30f7548ce2c6c791e6d4f8195b02afddedca60a9e7c77447702e728edc3  sddm-0.19.0.tar.xz
f0b4eb7ef0581701157f9decc637629156f36f6711b9a4bae517f94d7a1df614c81bbd891c918f07ac50e2a3d1519c43ccb9eefd80282c95dd79eca0e8d90904  pam-path-fix.patch
b61c02e9d75c03c5a04a44bf53bf1d7df3afe6214b2c7f6da8a2b3a78c859443fb2af6ff748a9d369707e6eaab8c0e5f9872c35e52e602836795b7099330ba00  utmpx.patch
51a0b6d427da8ea8dfbeb043ae4eb47f6c55645d03ed3a88e7583c216c52916863d8184faa6402f348e6ca704098b180079f9c6d4faaca1855bad58f9cf5f531  revert-fedora.patch
65fb8d772ca149f3616cb9d106eebf246b65bc8c0d2e328bd361dcc2664afb6ae467f698ac656653c5092f3b2b5f14b2980f9f1f3929839bb0674c5c801b36a1  xephyr.patch
d47123e097e8ea918ba112332272dbc94a3f06a76dc01080fd80f5f945320e0647d8f84d4232d32650c9705e2228c054748ce7207492a4c7e6039655668a0440  retry.patch
5dd057acc054f905ad0b9b0924eeb792b417cfe9d506f1d138266b9dc38c229b1b32d0c2443e85db5bba90d8c64efcebfceac07bd742acf0d8c10d992e8b4e1a  cleanup-xorg.patch
fea260015dfaf82e97ea2a77d7810e11f85503446ce6450ed9fe3072ce2151bb2ecac56fbc917a47b8ebcaf9fa51c077d4e798c4ef62919e18d1751fd7de92fc  authed.patch
829397bdb1d5fd6887b7836d503b081a8827346aa775b702d4ea9946d3ff4374446175e462732e52e33e1a53481b99cbe652c44e031c79823d5fbf67e4b48c46  allocate-vt.patch
378a735946c93dc9fb746cc4d4cf436d728ec0f1fddd3fcac8797ef90acb0d6582e41ac0a91b96d3886695b8664627ef81822f531c67deb12085626206f27136  autologin-type.patch
327d3f1d35d493cc302096d7047d0159e570c94320df52efe7e4cefeff910b38e322eaba57396e91efed7f9fb2da5ccf19b383cbfbd14537e016c1dfa319bd8e  autologin-pam.patch
0997ae1ea2f0b44835a397f107d73ae2eb0d492cbf97b3f446c388eebb6ec0b9255b2352a51c9e00ebf846c0fec61998895aba865e06814ee20ab9d526b3f353  session-env.patch
77d1abe99a8f0c38f9bf2d1ec695d371a46842b7f64c0a53e8f5c34550de5f9fba7ae9b187ba698e6658f03b18c6bd4b975532d9f2cecdb857e42e20413e96a7  qt515.patch
d603934552bad47edda458d7a4df2310e98bde74bdb3bf8588f5171b2a5d68814192b8dc8f5599b35402f9a747d519d985d4976e7aa50dabed445f99a112594c  sddm.initd"
