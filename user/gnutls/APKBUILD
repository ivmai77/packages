# Contriburo: Łukasz Jendrysik <scadu@yandex.com>
# Contributor: Michael Mason <ms13sp@gmail.com>
# Maintainer: Zach van Rijn <me@zv.io>
pkgname=gnutls
pkgver=3.7.4
pkgrel=0
pkgdesc="A TLS protocol implementation"
url="https://www.gnutls.org/"
arch="all"
license="LGPL-2.1+"
makedepends="libtasn1-dev libunistring-dev nettle-dev p11-kit-dev texinfo
	unbound-dev cmd:which zlib-dev"
checkdepends="diffutils"
subpackages="$pkgname-dbg $pkgname-doc $pkgname-dev $pkgname-lang
	$pkgname-c++:xx $pkgname-dane $pkgname-utils"
_v=${pkgver%.*}
_v2=$pkgver
case $pkgver in
*.*.*.*)
	_v=${_v%.*}
	_v2=${pkgver%.*}
	;;
esac
source="https://www.gnupg.org/ftp/gcrypt/gnutls/v${_v}/$pkgname-$pkgver.tar.xz
	disable-certtool-test.patch
	"

# secfixes:
#   3.5.13-r0:
#     - CVE-2017-7507
#   3.6.14-r0:
#     - CVE-2020-11501
#     - CVE-2020-13777
#   3.6.15-r0:
#     - CVE-2020-24659

build() {
	LIBS="-lgmp" ./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--disable-openssl-compatibility \
		--disable-rpath \
		--disable-static \
		--disable-guile \
		--disable-valgrind-tests \
		--without-included-libtasn1 \
		--enable-cxx \
		--enable-manpages \
		--enable-tests \
		--disable-full-test-suite \
		--with-p11-kit
	make
}

check() {
	make check
}

package() {
	make -j1 DESTDIR="$pkgdir" install
}

dane() {
	pkgdesc="$pkgdesc (DNS DANE support library)"
	mkdir -p "$subpkgdir"/usr/bin
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/bin/danetool "$subpkgdir"/usr/bin/
	mv "$pkgdir"/usr/lib/libgnutls-dane* "$subpkgdir"/usr/lib/
}

utils() {
	pkgdesc="Command line tools for TLS protocol"
	mkdir -p "$subpkgdir"/usr/
	mv "$pkgdir"/usr/bin "$subpkgdir"/usr/
}

xx() {
	pkgdesc="The C++ interface to GnuTLS"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/lib*xx.so.* "$subpkgdir"/usr/lib/
}

sha512sums="38b488ca1223d9aa8fc25756df08db6f29aaf76fb5816fdeaa14bd89fb431a2e1c495fefc64094f726337d5b89e198146ec7dc22e9a1bca6841a9d881b0d99e6  gnutls-3.7.4.tar.xz
510b0fd3464592cfffefa6cbfd09935aa26ab057b9dab7e588527efa2373a6f1ff8344270b4c143c669697e1bfa14218da29730dbc14e27daa21f770a519af1b  disable-certtool-test.patch"
