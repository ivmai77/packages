# Contributor: Síle Ekaterin Liszka <sheila@vulpine.house>
# Maintainer: Síle Ekaterin Liszka <sheila@vulpine.house>
pkgname=qps
pkgver=2.3.0
_lxqt_build=0.9.0
pkgrel=0
pkgdesc="Qt-based task manager"
url="https://lxqt.github.io/"
arch="all"
options="!check"  # No test suite.
license="GPL-2.0+"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtx11extras-dev
	qt5-qttools-dev libxrender-dev lxqt-build-tools>=$_lxqt_build
	liblxqt-dev kwindowsystem-dev"
subpackages="$pkgname-doc"
source="https://github.com/lxqt/qps/releases/download/$pkgver/qps-$pkgver.tar.xz
	qps-1.10.17-compile-fixes.patch
	"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} -Bbuild
	make -C build
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE make -C build test
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="1b884088fbdfeeaf4814453df4f7a345ebbb1825da0c09e768a655e8266f2089e7c2b9cca5f8d84dc0316a97d884a91ca5b8d9c2336a195c14432302db71171a  qps-2.3.0.tar.xz
32c154f1d3c9e74dda3eabfd2e3b9e9f3c1d77f808dfacc9dd05c904066832d92d361ca56ef9d784d945fad60cf67d6c909cbb730a0ffed1fea3bf44c3aad5a2  qps-1.10.17-compile-fixes.patch"
