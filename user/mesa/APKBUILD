# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=mesa
pkgver=21.3.8
_llvmver=14
pkgrel=0
pkgdesc="Mesa DRI OpenGL library"
url="https://www.mesa3d.org"
arch="all"
options="!check"  # No test suite.
license="MIT AND X11 AND SGI-B-2.0 AND BSL-1.0 AND LGPL-2.1+"
depends=""
depends_dev="libdrm-dev libxcb-dev libxdamage-dev libxext-dev libxshmfence-dev
	xorgproto-dev"
makedepends="$depends_dev bison eudev-dev expat-dev flex libelf-dev
	libva-dev libvdpau-dev libx11-dev libxfixes-dev libxrandr-dev libxt-dev
	libxv-dev libxvmc-dev libxxf86vm-dev llvm$_llvmver-dev makedepend meson
	ninja python3 py3-libxml2 py3-mako py3-pygments py3-markupsafe
	wayland-dev wayland-protocols
	zlib-dev zstd-dev"
subpackages="$pkgname-dev $pkgname-dri
	$pkgname-glapi $pkgname-egl $pkgname-gl $pkgname-gles
	$pkgname-xatracker $pkgname-osmesa $pkgname-gbm"
#	requires glslang: $pkgname-vulkan-overlay
source="https://mesa.freedesktop.org/archive/mesa-$pkgver.tar.xz
	big-endian-flipping.patch
	musl-fixes.patch
	no-tls.patch
	"

_dri_driverdir=/usr/lib/xorg/modules/dri
_dri_drivers="r200,nouveau"
_gallium_drivers="r300,r600,radeonsi,nouveau,swrast,virgl"
_vulkan_drivers="amd"

case "$CARCH" in
x86* | pmmx)
	_dri_drivers="${_dri_drivers},i965"
	_gallium_drivers="${_gallium_drivers},i915,svga,iris"
	_vulkan_drivers="${_vulkan_drivers},intel"
	;;
aarch64 | arm*)
	_gallium_drivers="${_gallium_drivers},vc4,freedreno,tegra,kmsro,v3d,lima,panfrost,etnaviv"
	case "$CARCH" in
	armhf) CFLAGS="$CFLAGS -mfpu=neon";;
	esac
	;;
ppc64)
	_arch_conf="-Dpower8=disabled"
	;;
esac

build() {
	meson \
		-Dprefix=/usr \
		-Ddri-drivers-path=$_dri_driverdir \
		-Dplatforms=x11,wayland \
		-Ddri-drivers=$_dri_drivers \
		-Dgallium-drivers=$_gallium_drivers \
		-Dvulkan-drivers=$_vulkan_drivers \
		-Dosmesa=true \
		$_arch_conf \
		build

	ninja -C build
}

package() {
	DESTDIR="$pkgdir" ninja -C build install
}

egl() {
	replaces="mesa"
	pkgdesc="Mesa libEGL runtime libraries"
	install -d "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libEGL.so* \
		"$subpkgdir"/usr/lib/
}

gl() {
	replaces="mesa"
	pkgdesc="Mesa libGL runtime libraries"
	install -d "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libGL.so* \
		"$subpkgdir"/usr/lib/
}

glapi() {
	replaces="$pkgname-gles"
	pkgdesc="Mesa OpenGL API"
	install -d "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libglapi.so.* \
		"$subpkgdir"/usr/lib/
}

gles() {
	replaces="mesa"
	pkgdesc="Mesa libGLESv2 runtime libraries"
	install -d "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libGLES*.so* \
		"$subpkgdir"/usr/lib/
}

xatracker() {
	pkgdesc="Mesa XA state tracker for VMware"
	install -d "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libxatracker*.so.* \
		"$subpkgdir"/usr/lib/
}

osmesa() {
	pkgdesc="Mesa offscreen rendering libraries"
	install -d "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libOSMesa.so.* \
		"$subpkgdir"/usr/lib/
}

gbm() {
	pkgdesc="Mesa GBM library"
	replaces="mesa"
	install -d "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/libgbm.so.* \
		"$subpkgdir"/usr/lib/
}

vulkan_overlay() {
	pkgdesc="Vulkan layer to display information about the current window"
	install -d "$subpkgdir"/usr/lib
	install -d "$subpkgdir"/usr/share/vulkan/explicit_layer.d
	mv "$pkgdir"/usr/lib/libVkLayer_MESA_overlay.so \
		"$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/share/vulkan/explicit_layer.d/VkLayer_MESA_overlay.json \
		"$subpkgdir"/usr/share/vulkan/explicit_layer.d/
}

dri() {
	pkgdesc="Mesa DRI drivers"
	replaces="mesa-dri-ati mesa-dri-freedreno mesa-dri-intel
	mesa-dri-nouveau mesa-dri-swrast mesa-dri-tegra mesa-dri-vc4
	mesa-dri-virtio mesa-dri-vmwgfx"
	provides="$replaces"
	install -d "$subpkgdir"/usr/lib/xorg/modules
	install -d "$subpkgdir"/usr/share/vulkan
	mv "$pkgdir"/usr/lib/dri "$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/lib/xorg/modules/dri "$subpkgdir"/usr/lib/xorg/modules/
	mv "$pkgdir"/usr/share/drirc.d "$subpkgdir"/usr/share/
	# XvMC drivers
	mv "$pkgdir"/usr/lib/libXvMC*.so* "$subpkgdir"/usr/lib/
	# support non-Vulkan arches
	mv "$pkgdir"/usr/lib/libvulkan*.so* "$subpkgdir"/usr/lib/ || true
	mv "$pkgdir"/usr/lib/vdpau "$subpkgdir"/usr/lib/
	mv "$pkgdir"/usr/share/vulkan/icd.d "$subpkgdir"/usr/share/vulkan/
}

sha512sums="0462c44fa8e358dafd03088411452bc172a365902795b3198df1e6cfbf1d87675ef5b72b572de7f15e5ee87b30ce5b2f217c81ea72b376962f6428c6ff83f2e9  mesa-21.3.8.tar.xz
3417e5c6d7ec564178e1d72431042b0a6ba659321f13a3dda81eda5fa0f2c8bc7c6972cb8266aea84ab05976ffb161659e9988c50ecc418e8bc1e1ce8f93a65f  big-endian-flipping.patch
9f7a050f09571a2b17098d495b82e2e85b293fb7285e7d6d7c3c48cd4220a1bdcc61a7321ba78dd14860939ecabe7e89b32d6110f3728f793273e1e26b78a553  musl-fixes.patch
076bb17d85235f3c833af8f1641f3556e406ad187b7ae1ebacced5f57b8832243a878678de4e50880c9a50b2aae3c42ad2342ed8c3e18d881edb7dd5018a710e  no-tls.patch"
