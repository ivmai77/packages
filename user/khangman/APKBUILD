# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=khangman
pkgver=22.04.2
pkgrel=0
pkgdesc="Hangman word game"
url="https://www.kde.org/applications/education/khangman/"
arch="all"
license="GPL-2.0-only"
depends=""
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qtdeclarative-dev
	qt5-qtsvg-dev ki18n-dev kcrash-dev kcompletion-dev kconfig-dev kio-dev
	kcoreaddons-dev kconfigwidgets-dev kdeclarative-dev kdoctools-dev
	knewstuff-dev knotifications-dev kxmlgui-dev libkeduvocdocument-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/khangman-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="52670c7fc654de76a5cc24e2f6fafdd40e658d7ce118ffe6f86855865a9d71ade95c1910ed208bcb33a2710831ba5e5556031afc3015025e2e028f0e68d85a8a  khangman-22.04.2.tar.xz"
