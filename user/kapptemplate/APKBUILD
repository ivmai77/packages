# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kapptemplate
pkgver=22.04.2
pkgrel=0
pkgdesc="KDE application code template generator"
url="https://kde.org/applications/development/org.kde.kapptemplate"
arch="all"
license="GPL-2.0+"
depends=""
makedepends="qt5-qtbase-dev cmake extra-cmake-modules karchive-dev
	kcompletion-dev kconfigwidgets-dev kcoreaddons-dev kdoctools-dev
	ki18n-dev kio-dev kjobwidgets-dev kservice-dev solid-dev kauth-dev
	kcodecs-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kapptemplate-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="e05e5a20dfba604f82336f009994928d1a31f120fe2180899298f6a71d9739c94e399b5ddb90e94aeb8b2746dd2e42e290e62a37fc12320cf42ade3d015edbdf  kapptemplate-22.04.2.tar.xz"
