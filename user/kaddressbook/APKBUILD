# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kaddressbook
pkgver=22.04.2
pkgrel=0
pkgdesc="KDE contact manager"
url="https://kontact.kde.org/components/kaddressbook.html"
arch="all"
license="GPL-2.0+"
depends=""
makedepends="qt5-qtbase-dev cmake extra-cmake-modules akonadi-dev boost-dev
	akonadi-contacts-dev akonadi-mime-dev akonadi-search-dev gpgme-dev
	grantlee-dev grantleetheme-dev kauth-dev kcalendarcore-dev
	kcmutils-dev kcodecs-dev kcompletion-dev kconfigwidgets-dev
	kcontacts-dev kcoreaddons-dev kcrash-dev kdbusaddons-dev kdoctools-dev
	kimap-dev kio-dev kitemmodels-dev kitemviews-dev kjobwidgets-dev
	kmime-dev kontactinterface-dev kparts-dev kpimtextedit-dev kservice-dev
	kuserfeedback-dev kwidgetsaddons-dev kwindowsystem-dev kxmlgui-dev
	libkdepim-dev libkleo-dev pimcommon-dev prison-dev solid-dev sonnet-dev"
subpackages="$pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/kaddressbook-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} \
		.
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="65085b413f6dbe037a5aba5a3f349882879197da85459c90dbf6777360a8405dbf2d79dac2994b28f682f81b4994550c21940e180f244dcdbaacdbd50bf8d140  kaddressbook-22.04.2.tar.xz"
