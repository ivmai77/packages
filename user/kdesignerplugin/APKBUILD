# Contributor: A. Wilcox <awilfox@adelielinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=kdesignerplugin
pkgver=5.94.0
pkgrel=0
pkgdesc="Qt Designer plugin for KDE widgets"
url="https://www.kde.org/"
arch="all"
options="!check"  # Test requires accelerated X11 display.
license="LGPL-2.1-only"
depends=""
depends_dev="$pkgname=$pkgver-r$pkgrel"
makedepends="cmake extra-cmake-modules qt5-qtbase-dev qt5-qttools-dev
	kconfig-dev kcoreaddons-dev kdoctools-dev"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/portingAids/kdesignerplugin-$pkgver.tar.xz"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=RelWithDebugInfo \
		-DCMAKE_CXX_FLAGS="$CXXFLAGS" \
		-DCMAKE_C_FLAGS="$CFLAGS" \
		${CMAKE_CROSSOPTS} .
	make
}

check() {
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="46651ed770da8070541d8333baf6e37059693a4407245ea7330c8724f1ad03b06cbccea20b3909dbdd33ddaa33e9d2eac45d031eac8e0cc23eca0cec20a1e2ad  kdesignerplugin-5.94.0.tar.xz"
