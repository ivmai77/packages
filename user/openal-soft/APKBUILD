# Contributor: Ariadne Conill <ariadne@dereferenced.org>
# Maintainer: Zach van Rijn <me@zv.io>
pkgname=openal-soft
pkgver=1.19.1
pkgrel=0
pkgdesc="Software implementation of OpenAL API"
url="https://kcat.strangesoft.net/openal.html"
arch="all"
options="!check"  # No test suite.
license="LGPL-2.0+"
depends=""
depends_dev="alsa-lib-dev"
makedepends="$depends_dev cmake linux-headers pulseaudio-dev"
subpackages="$pkgname-dev"
source="https://openal-soft.org/openal-releases/${pkgname}-${pkgver}.tar.bz2"

build() {
	local _cpuext_neon=""
	case "$CARCH" in
		armhf) _cpuext_neon="-DALSOFT_CPUEXT_NEON=OFF" ;;
	esac
	cmake \
		-DCMAKE_INSTALL_PREFIX="/usr" \
		-DCMAKE_INSTALL_LIBDIR="lib" \
		$_cpuext_neon \
		-Bbuild
	make -C build
}

package() {
	make DESTDIR="$pkgdir" -C build install
}

sha512sums="356a3f12bbe284bfac71f4f2a9f83d084083a832012222e73743042ac27812cf051c12b0ddcf53c010ff77e44ae2e9033cea9b9324d316a22a59a154307a64a4  openal-soft-1.19.1.tar.bz2"
