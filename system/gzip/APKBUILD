# Contributor: Leonardo Arena <rnalrd@alpinelinux.org>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Dan Theisen <djt@hxx.in>
pkgname=gzip
pkgver=1.12
pkgrel=0
pkgdesc="The GNU data compression program"
subpackages="$pkgname-doc"
url="https://www.gnu.org/software/gzip/"
arch="all"
license="GPL-3.0+"
depends=""
makedepends=""
checkdepends="perl coreutils diffutils less"
source="https://ftp.gnu.org/gnu/gzip/gzip-$pkgver.tar.gz"

build() {
	# avoid text relocation
	export DEFS="NO_ASM"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info
	make
}

check() {
	make check
}

package() {
	make DESTDIR=$pkgdir install

	rm -rf "$pkgdir"/usr/lib/charset.alias
	rmdir -p "$pkgdir"/usr/lib 2>/dev/null || true

	mkdir -p "$pkgdir"/bin
	mv "$pkgdir"/usr/bin/gzip "$pkgdir"/usr/bin/gunzip "$pkgdir"/bin/
	ln -s /bin/gzip "$pkgdir"/usr/bin/gzip
	ln -s /bin/gunzip "$pkgdir"/usr/bin/gunzip

	# http://bugs.alpinelinux.org/issues/4011
	ln -sf /bin/gunzip "$pkgdir"/usr/bin/uncompress
}

sha512sums="09b441299039479488700b7ef267ab7f71268af0d648d32cf6b1efcac58e59f1f352fa9d4f95278e96a76dc21239be1a5acab319bc85a7501cf5d8573c83d857  gzip-1.12.tar.gz"
