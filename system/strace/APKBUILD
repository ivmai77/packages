# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=strace
pkgver=5.17
pkgrel=0
pkgdesc="A useful diagnositic, instructional, and debugging tool"
url="https://strace.io/"
arch="all"
options="!check"  # test suite requires mpers, which requires multilib
license="LGPL-2.1+"
depends=""
makedepends="linux-headers autoconf automake"
subpackages="$pkgname-doc"
source="https://github.com/strace/strace/releases/download/v$pkgver/strace-$pkgver.tar.xz
	nlattr-fix.patch
	"

build() {
	case "$CLIBC" in
	musl) export CFLAGS="$CFLAGS -Dsigcontext_struct=sigcontext" ;;
	esac

	case "$CARCH" in
	s390x)
		# __SIGNAL_FRAMESIZE is defined in asm/sigcontext.h
		# but including it would make conflict with struct sigcontext
		# since we compile with it in musl.
		# Temporarily add this until musl upstream has a proper fix
		# for struct sigcontext.
		export CFLAGS="$CFLAGS -D__SIGNAL_FRAMESIZE=160"
		;;
	esac

	ac_cv_have_long_long_off_t=yes \
	ac_cv_header_sys_ipc_h=no \
	ac_cv_header_sys_msg_h=no \
	ac_cv_header_sys_sem_h=no \
	ac_cv_header_sys_shm_h=no \
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--disable-mpers
	make
}

check() {
	make check
}

package() {
	make -j1 DESTDIR="$pkgdir" install
}

sha512sums="1b63cf7e6e339333b9d24fa20232409192abc815f2ebe2e336ef4acc039cd06c976b3c12e9ce993491a0e6b86c26b90ceba962f580e894b1ff5ab9863bdfcc44  strace-5.17.tar.xz
6616161b6e015c5e56b7171995d28ab63a865156b7f9826b4be26beaac863f0ebc341014910ea53157a810c6afafc10ce80b2d31f4d649d28718a9be78795c6c  nlattr-fix.patch"
