# Maintainer: Zach van Rijn <me@zv.io>
pkgname=fakeroot
pkgver=1.28
pkgrel=0
pkgdesc="Tool for simulating superuser privileges"
arch="all"
license="GPL-3.0+"
url="https://wiki.debian.org/FakeRoot"
depends=""
makedepends_build="libtool autoconf automake po4a"
makedepends_host="libcap-dev acl-dev linux-headers"
makedepends="$makedepends_build $makedepends_host"
checkdepends="bash sharutils"
subpackages="$pkgname-doc"
# find timestamp here: https://snapshot.debian.org/package/fakeroot/
source="https://snapshot.debian.org/archive/debian/20220304T204941Z/pool/main/f/$pkgname/${pkgname}_${pkgver}.orig.tar.gz
	fakeroot-no64.patch
	fakeroot-stdint.patch
	xstatjunk.patch

	fix-prototype-generation.patch
	"

build() {
	# musl does not have _STAT_VER, it's really not used for
	# anything, so define it as zero
	# define _ID_T so libfakeroot knows headers define it
	export CFLAGS="-D_STAT_VER=0 -D_ID_T $CFLAGS"

	./bootstrap
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--disable-static

	make
	cd doc
	po4a -k 0 --rm-backups --variable "srcdir=../doc/" po4a/po4a.cfg
}

check() {
	make check VERBOSE=x
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="cd55007014da5741ff336d005a80633a2f1ed856e075162acb9a4a1edd5c6d17ec74457c8a1e31edb02ae70920904c53e69365d55bd9beb7e8c6211aa8cfca8b  fakeroot_1.28.orig.tar.gz
7a832e6bed3838c7c488e0e12ba84b8d256e84bbb06d6020247452a991de505fa5c6bd7bcb84dce8753eb242e0fcab863b5461301cd56695f2b003fe8d6ff209  fakeroot-no64.patch
ed7a58b0d201139545420f9e5429f503c00e00f36dea84473e77ea99b23bb8d421da1a8a8ce98ff90e72e378dff4cb9ea3c1a863a969899a5f50dfac3b9c5fac  fakeroot-stdint.patch
5efd33fd778bd94a529ed7e439fb8fea25ff865dda3f6f9e431264e942b37f3b5d7a0ad14107b55c5fa81b86efd5a82aedb3803cfab08ec57f27f5b229d2fe88  xstatjunk.patch
63db66b0d883495151f817c4e57bdaaff992667416f2f5b03c9b66b65f1fba1762f709dd5153717aa4008d7be0fbc58bf1f41eb2b35dc61047b0cc0b762e145b  fix-prototype-generation.patch"
