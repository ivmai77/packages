# Contributor: Sergey Lukin <sergej.lukin@gmail.com>
# Contributor: Łukasz Jendrysik <scadu@yandex.com>
# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Síle Ekaterin Liszka <sheila@vulpine.house>
pkgname=vim
pkgver=9.0.1385
_pkgver=${pkgver%.*.*}${pkgver#*.}
_pkgver=${_pkgver%.*}
pkgrel=0
pkgdesc="advanced text editor"
url="https://www.vim.org"
arch="all"
options="!check"  # requires controlling TTY, and fails with musl locales
license="Vim"
depends=""
makedepends_host="acl-dev ncurses-dev"
[ "$CBUILD" != "$CHOST" ] || makedepends_host="$makedepends_host perl-dev python3-dev"
subpackages="$pkgname-doc ${pkgname}diff::noarch"
source="$pkgname-$pkgver.tar.gz::https://github.com/$pkgname/$pkgname/archive/v$pkgver.tar.gz
	vimrc
	"
builddir="$srcdir/$pkgname-$pkgver"

# secfixes:
#   9.0.1385-r0:
#     - CVE-2023-0049
#     - CVE-2023-0051
#     - CVE-2023-0054
#     - CVE-2023-0288
#     - CVE-2023-0433
#     - CVE-2023-0512
#   8.0.0329-r0:
#     - CVE-2017-5953
#   8.0.0056-r0:
#     - CVE-2016-1248

prepare() {
	default_prepare
	# Read vimrc from /etc/vim
	echo '#define SYS_VIMRC_FILE "/etc/vim/vimrc"' >> src/feature.h
}

build() {
	[ "$CBUILD" != "$CHOST" ] || _onlynative="--enable-perlinterp=dynamic --enable-python3interp=dynamic"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		$_onlynative \
		--without-x \
		--enable-acl \
		--enable-nls \
		--enable-multibyte \
		--enable-gui=no \
		--with-compiledby="Adélie Linux" \
		vim_cv_toupper_broken=no \
		vim_cv_terminfo=yes \
		vim_cv_tgetent=zero \
		vim_cv_tty_group=world \
		vim_cv_getcwd_broken=no \
		vim_cv_stat_ignores_slash=no \
		vim_cv_memmove_handles_overlap=yes \
		STRIP=:
	make
}

package() {
	make -j1 DESTDIR="$pkgdir/" install

	install -Dm644 runtime/doc/uganda.txt \
		"$pkgdir/usr/share/licenses/$pkgname/LICENSE"
	install -Dm644 "$srcdir"/vimrc "$pkgdir"/etc/vim/vimrc

	# the following properly belong to gvim.
	rm -r "$pkgdir"/usr/share/applications
	rm "$pkgdir"/usr/share/vim/vim$_pkgver/gvimrc_example.vim
	rm -r "$pkgdir"/usr/share/icons
}

vimdiff() {
	pkgdesc="view file diffs in vim"
	depends="diffutils"

	install -d "$subpkgdir"/usr/bin
	mv "$pkgdir"/usr/bin/vimdiff "$subpkgdir"/usr/bin
}

sha512sums="807f53451a5d6a38eb35c1a79c8dbd7a54e74b6e3f4f93099900579668254948231c56a97bcd9cbb3d9ba4f63a8042062395351dcfb23fd7e390f8d8eb97c106  vim-9.0.1385.tar.gz
12ee3f96c94d74215159fba379ed61907ec5982a9f1643575dcb7c3d5e30824665d683de95f97b5067718b3f2a1238fb7534a70803bc170614498ad026f352d8  vimrc"
