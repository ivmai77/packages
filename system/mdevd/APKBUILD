# Contributor: Laurent Bercot <ska-adelie@skarnet.org>
# Maintainer: Laurent Bercot <ska-adelie@skarnet.org>
pkgname=mdevd
pkgver=0.1.6.2
pkgrel=0
pkgdesc="A small uevent manager daemon"
url="https://skarnet.org/software/$pkgname/"
arch="all"
options="!check"  # No test suite.
license="ISC"
_skalibs_version=2.13
makedepends="skalibs-dev>=$_skalibs_version"
subpackages="$pkgname-doc $pkgname-openrc"
source="https://skarnet.org/software/$pkgname/$pkgname-$pkgver.tar.gz mdev.conf mdevd.run mdevd.initd"

build() {
	./configure --enable-allstatic --enable-static-libc
	make
}

package() {
	make DESTDIR="$pkgdir" install
	mkdir -p -m 0755 "$pkgdir/etc/init.d"
	cp -f "$srcdir/mdev.conf" "$pkgdir/etc/"
	chmod 0644 "$pkgdir/etc/mdev.conf"
	cp -f "$srcdir/mdevd.initd" "$pkgdir/etc/init.d/mdevd"
	chmod 0755 "$pkgdir/etc/init.d/mdevd"
}


doc() {
        pkgdesc="$pkgdesc (documentation)"
        depends=
        install_if="docs $pkgname=$pkgver-r$pkgrel"
        mkdir -p "$subpkgdir/usr/share/doc"
        cp -a "$builddir/doc" "$subpkgdir/usr/share/doc/$pkgname"
}


openrc() {
	rldir="$subpkgdir"/etc/runlevels/sysinit
	svcdir="$subpkgdir/etc/s6-linux-init/current/run-image/service/mdevd"
        default_openrc
        mkdir -p "$rldir" "$svcdir"
	cp -f "$srcdir/mdevd.run" "$svcdir/run"
	chmod 0755 "$svcdir/run"
	echo 3 > "$svcdir/notification-fd"
	touch "$svcdir/down"
        ln -s ../../init.d/mdevd "$rldir/mdevd"
}

sha512sums="e1b0aee398715ab8892518efb41a983114b8bc245374b3ee9dc2bd652ae8545ac2d191ea43e7a8dda84e995dc3d30dad93ef14147a4fa3e0f20e80f72b232887  mdevd-0.1.6.2.tar.gz
f966d66366eac3b9b9eeb9b8523ea0924ada2b858c85ca0c0151d0fb374dfbf56c49ec2210d2d5ca19aa4d9f24371c85d777050eb8bf9f57821ec65704f18717  mdev.conf
427a5903fa2126060955dcce8144d59255a679c4973f2dbc3145a4d646e879fc241ebcaa53289498719d343c746fc376c41defa87932dcbe91192b2d6f4ed1c4  mdevd.run
e7599f51a4280243a5be459c6fad7eb8ba3b5f65fae8cad923ccca2addab55787819909fea311c998e1126e6802a81ab000ee6de7474f3245ce72521244c22ba  mdevd.initd"
