# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=xfsprogs
pkgver=5.7.0
pkgrel=0
pkgdesc="XFS filesystem utilities"
url="https://xfs.org/index.php/Main_Page"
arch="all"
options="!check"  # No test suite.
license="GPL-1.0-only"
depends="$pkgname-base"
makedepends="attr-dev bash icu-dev libedit-dev linux-headers util-linux-dev"
subpackages="$pkgname-base $pkgname-dev $pkgname-doc $pkgname-lang $pkgname-libs"
source="https://www.kernel.org/pub/linux/utils/fs/xfs/$pkgname/$pkgname-$pkgver.tar.gz
	fix-mmap.patch
	no-utmp-header.patch
	"

build() {
	export DEBUG=-DNDEBUG
	export OPTIMIZER="$CFLAGS"
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sbindir=/sbin \
		--libexecdir=/usr/lib \
		--enable-editline
	make SHELL=/bin/bash
}

check() {
	make check
}

package() {
	make -j1 DIST_ROOT="$pkgdir" install install-dev
	find "$pkgdir" -name *.la -delete
	chown -R 0:0 "$pkgdir"
}

base() {
	# Everything except xfs_scrub, which pulls in 50 MiB of icu libs.
	pkgdesc="Base $pkgdesc"
	mkdir "$subpkgdir"
	mv "$pkgdir"/sbin "$subpkgdir"/
	mkdir "$pkgdir"/sbin
	mv "$subpkgdir"/sbin/xfs_scrub "$pkgdir"/sbin/
}

sha512sums="81e0be0aad0024e7b36c93de30a737677bb244510b4f1e010838dfad900b2b96d59d500e3b89c6e1f9b64c5f86ef7b502a676b8f5c3d064a9e8e75ec69f61785  xfsprogs-5.7.0.tar.gz
c23d5dca744c4589ede517701fc8ea02f9b7a59568d907269048741806d2e6c9e56ed3493163d63dbf16193ff99471206548b25efcda18e3e5dff14eb38066d4  fix-mmap.patch
29c77c550fa8f7a0b3794649d278a2cb3a65c9368db19415866910160adb6d5a52f1be4f8713b58c5c87f149b6212c068ae27a4547a6c4e4fe7b1584e1261dae  no-utmp-header.patch"
