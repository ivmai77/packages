# Contributor: Carlo Landmeter <clandmeter@gmail.com>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=libxml2
pkgver=2.10.3
pkgrel=1
pkgdesc="XML parsing library"
url="https://gitlab.gnome.org/GNOME/libxml2/-/wikis/home"
arch="all"
options="!strip"
license="MIT"
depends=""
depends_dev="zlib-dev icu-dev"
checkdepends="perl libarchive"
makedepends="$depends_dev python3-dev"
subpackages="$pkgname-doc $pkgname-dev py3-libxml2:py"
provides="$pkgname-utils=$pkgver-r$pkgrel"
source="https://download.gnome.org/sources/libxml2/${pkgver%.*}/libxml2-$pkgver.tar.xz
	python-segfault-fix.patch
	"

# secfixes:
#   2.10.0-r0:
#     - CVE-2022-29824
#     - CVE-2022-2309
#   2.9.4-r1:
#     - CVE-2016-5131
#   2.9.4-r2:
#     - CVE-2016-9318
#   2.9.4-r4:
#     - CVE-2017-5969
#   2.9.10-r1:
#     - CVE-2019-20388
#     - CVE-2020-7595

prepare() {
	default_prepare
}

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--with-icu \
		--without-iconv \
		--with-python=/usr/bin/python3 \
		--enable-static
	make
}

check() {
	make check
}

package() {
	make -j1 DESTDIR="$pkgdir" install
}

py() {
	pkgdesc="$pkgname Python bindings"
	install -d "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/python3* "$subpkgdir"/usr/lib/
}

sha512sums="33bb87ae9a45c475c3de09477e5d94840d8f687f893ef7839408bc7267e57611c4f2b863ed8ec819a4b5f1ebd6a122db9f6054c73bceed427d37f3e67f62620c  libxml2-2.10.3.tar.xz
384b3d2031cd8f77528190bbb7652faa9ccb22bc604bcf4927e59046d38830dac38010828fe1568b6514976f725981a6d3ac1aa595d31477a36db2afe491452c  python-segfault-fix.patch"
