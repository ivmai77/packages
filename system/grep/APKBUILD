# Contributor: Michael Mason <ms13sp@gmail.com>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: A. Wilcox <awilfox@adelielinux.org>
pkgname=grep
pkgver=3.10
pkgrel=0
pkgdesc="Pattern matching utilities"
url="https://www.gnu.org/software/grep/grep.html"
arch="all"
license="GPL-3.0+"
depends=""
checkdepends="coreutils bash diffutils"
makedepends="pcre-dev autoconf automake"
subpackages="$pkgname-doc $pkgname-lang"
source="https://mirrors.kernel.org/gnu/$pkgname/$pkgname-$pkgver.tar.xz
	gnulib-tests-dont-require-gpg-passphrase.patch
	fix-tests.patch
	"

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--bindir=/bin \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install

	rm -rf "$pkgdir"/usr/lib/charset.alias
	rmdir -p "$pkgdir"/usr/lib 2>/dev/null || true
}

sha512sums="865e8f3fd7afc68f1a52f5e1e1ee05fb9c6d6182201efb0dbdf6075347b0b1d2bf0784537a8f8dd4fb050d523f7a1d2fb5b9c3e3245087d0e6cc12d6e9d3961b  grep-3.10.tar.xz
7e4bc1da5de16a036e00fef6d9387b701bbe447d21d77cc3fc28a73e0364c972dec7cdcd70a176ef339b221fad92e7feccbb1e20f3f7b114a3585b8551770de5  gnulib-tests-dont-require-gpg-passphrase.patch
9ba6b01c0c74933299afb469dadd2ea0c7e24befa34c691671a576063e32a1f0c735541e5e2bb0073d8afd814790909f7f895827aa8a2fbacdfcae380a7bcb11  fix-tests.patch"
