# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Dan Theisen <djt@hxx.in>
pkgname=net-tools
pkgver=2.10
pkgrel=0
pkgdesc="Linux networking base tools"
url="https://sourceforge.net/projects/net-tools/"
arch="all"
options="!check"  # there is no testsuite for this package
license="GPL-2.0+"
depends="mii-tool"
depends_dev=""
makedepends="$depends_dev bash linux-headers"
subpackages="$pkgname-doc $pkgname-lang $pkgname-dbg mii-tool:mii_tool"
source="https://downloads.sourceforge.net/project/$pkgname/$pkgname-$pkgver.tar.xz"

prepare() {
	default_prepare
	cat > config.make <<EOF
I18N=1
HAVE_AFUNIX=1
HAVE_AFINET=1
HAVE_AFINET6=1
# HAVE_AFIPX=0
# HAVE_AFATALK=0
# HAVE_AFAX25=0
HAVE_AFNETROM=1
# HAVE_AFROSE=0
# HAVE_AFX25=0
# HAVE_AFECONET=0
# HAVE_AFDECnet=0
# HAVE_AFASH=0
# HAVE_AFBLUETOOTH=1
HAVE_HWETHER=1
# HAVE_HWARC=0
HAVE_HWSLIP=1
HAVE_HWPPP=1
HAVE_HWTUNNEL=1
# HAVE_HWSTRIP=0
# HAVE_HWTR=0
# HAVE_HWAX25=0
# HAVE_HWROSE=0
HAVE_HWNETROM=1
# HAVE_HWX25=0
# HAVE_HWFR=0
# HAVE_HWSIT=0
# HAVE_HWFDDI=0
# HAVE_HWHIPPI=0
# HAVE_HWASH=0
# HAVE_HWHDLCLAPB=0
HAVE_HWIRDA=1
# HAVE_HWEC=0
# HAVE_HWEUI64=0
# HAVE_HWIB=0
HAVE_FW_MASQUERADE=1
HAVE_IP_TOOLS=1
HAVE_MII=1
EOF
	sed -n -e 's/^\(HAVE.*\)=\(.*\)/#define \1 \2/p' config.make > config.h
}

build() {
	make
}

package() {
	make DESTDIR="$pkgdir" install
}

mii_tool() {
	pkgdesc="Media-Independent Interface (MII) tool"
	depends=""
	mkdir -p "$subpkgdir"/sbin
	mv "$pkgdir"/sbin/mii-tool "$subpkgdir"/sbin/
}

sha512sums="aa8f631772da6c16b165065fc5929a206504f2dce19e74a4ffc680f09b40f3a9180bd4bbeaac67e0b32693358b01383bae4fbfcb6061180c130e9e0801d536bf  net-tools-2.10.tar.xz"
