# Contributor: Leonardo Arena <rnalrd@alpinelinux.org>
# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Contributor: Síle Ekaterin Liszka <sheila@vulpine.house>
# Maintainer: Adélie Perl Team <adelie-perl@lists.adelielinux.org>
pkgname=perl
pkgver=5.34.1
pkgrel=1
pkgdesc="Larry Wall's Practical Extraction and Report Language"
url="https://www.perl.org/"
arch="all"
options="!fhs"
license="Artistic-1.0-Perl OR GPL-1.0+"
depends=""
makedepends="bzip2-dev zlib-dev"
subpackages="$pkgname-doc $pkgname-dev"
source="https://www.cpan.org/src/5.0/perl-$pkgver.tar.gz
	musl-locale.patch
	musl-stack-size.patch
	zlib-test.patch
	fix-test-chris-williams-tcp-server.patch
	"

# secfixes:
#   5.34.1-r0:
#   - CVE-2021-36770
#   5.26.3-r0:
#   - CVE-2018-12015
#   - CVE-2018-18311
#   - CVE-2018-18312
#   - CVE-2018-18313
#   - CVE-2018-18314
#   5.26.1-r0:
#   - CVE-2017-12837
#   - CVE-2017-12883

builddir="$srcdir/$pkgname-$pkgver"

_privlib=/usr/share/perl5/core_perl
_archlib=/usr/lib/perl5/core_perl

prepare() {
	chmod +w "$builddir"/*.c
	default_prepare
	sed	-e 's/less -R/less/g' \
		-e 's/libswanted="\(.*\) nsl\(.*\)"/libswanted="\1\2"/g' \
		-i ./Configure

	# Ensure that we never accidentally bundle zlib or bzip2
	rm -rf cpan/Compress-Raw-Zlib/zlib-src
	rm -rf cpan/Compress-Raw-Bzip2/bzip2-src
	sed -i '/\(bzip2\|zlib\)-src/d' MANIFEST
}

build() {
	BUILD_ZLIB=0
	BUILD_BZIP2=0
	BZIP2_LIB=/usr/lib
	BZIP2_INCLUDE=/usr/inculde

	export BUILD_ZLIB BUILD_BZIP2 BZIP2_LIB BZIP2_INCLUDE
	./Configure -des \
		-Dcccdlflags='-fPIC' \
		-Dcccdlflags='-fPIC' \
		-Dccdlflags='-rdynamic' \
		-Dprefix=/usr \
		-Dprivlib=$_privlib \
		-Darchlib=$_archlib \
		-Dvendorprefix=/usr \
		-Dvendorlib=/usr/share/perl5/vendor_perl \
		-Dvendorarch=/usr/lib/perl5/vendor_perl \
		-Dsiteprefix=/usr/local \
		-Dsitelib=/usr/local/share/perl5/site_perl \
		-Dsitearch=/usr/local/lib/perl5/site_perl \
		-Dlocincpth=' ' \
		-Doptimize="$CFLAGS" \
		-Duselargefiles \
		-Dusethreads \
		-Duseshrplib \
		-Dd_semctl_semun \
		-Dman1dir=/usr/share/man/man1 \
		-Dman3dir=/usr/share/man/man3 \
		-Dinstallman1dir=/usr/share/man/man1 \
		-Dinstallman3dir=/usr/share/man/man3 \
		-Dman1ext='1' \
		-Dman3ext='3pm' \
		-Dcf_by='Adelie' \
		-Ud_csh \
		-Dusenm
	make libperl.so && make
}

check() {
	# https://perldoc.perl.org/perlhack.txt
	export LC_ALL=C
	export TEST_JOBS=${JOBS}
	export PERL_TEST_HARNESS_ASAP=1
	make -j${JOBS} test_harness_notty
}

package() {
	make DESTDIR="$pkgdir" install
	if [ -n "$(find $pkgdir/usr/local -type f)" ]; then
		error "files found under /usr/local"
	fi
	find "$pkgdir" -name '.*' -delete
	rm "$pkgdir"/usr/bin/xsubpp
}

dev() {
	mkdir -p "$subpkgdir"/usr/bin "$subpkgdir"/$_archlib "$subpkgdir"/$_privlib
	mv "$pkgdir"/$_archlib/Devel "$subpkgdir"/$_archlib/
	mv "$pkgdir"/$_privlib/Encode "$subpkgdir"/$_privlib/

	mv "$pkgdir"/usr/bin/h2xs \
		"$pkgdir"/usr/bin/perlivp \
		"$pkgdir"/usr/bin/enc2xs \
		"$subpkgdir"/usr/bin/

	default_dev
}

sha512sums="bf17f2c6fce1b046dce11450f28823fe5df6341f259ec351a4445768feb38706413be867a32c1fd916567657ca9f08c838030e02bb8a3bc1cd8a1350c007cf3a  perl-5.34.1.tar.gz
a78b5fb1a2b6f60b401329cfd2d8349d4fdcc19628cde0e9b840b82e1a02e705f7d7413fe206aa13ed714ab93a65b62ac3d85dfd526ca8048621d5d89b22f0ef  musl-locale.patch
c004d6612ec754e5947255a2e2d15b5581f187c32495aeeec9f4fa286919bd9f40c72b63db61e3f4004b09288af2063a6a14b67e5c289e9a8b23ebd7c216e16f  musl-stack-size.patch
bbb2beb49604982c42c833b229852bc8bca4e3498f97fcfbb3bf7de10dddbef3b21c917f333958c64feb99a65f4093773e33018ae0e91dadeecdf985ab0ed3a2  zlib-test.patch
1cf3365942a6447083e7487e3b79e1a57d3b08dad9cadb86f44bdda3bd7882fe5d71ebb4fab12a0663d4c0d4c1566d593be10ea46ecf892926528a9ae31328b9  fix-test-chris-williams-tcp-server.patch"
